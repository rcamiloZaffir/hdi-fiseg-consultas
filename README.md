# Proyecto REST base para Fiscalización de seguros #


### Arranque del servicio
```
mvn clean spring-boot:run
```

## Servicios

### Swagger

En un navegador ingresar a la URL <http://localhost:8091/swagger-ui/index.html>

### Servicio ping

Desde la linea de comandos, ejecutar:
```
curl http://localhost:8091/api/ping
```

### Servicio de salud

Desde la linea de comandos, ejecutar:
```
curl http://localhost:8091/api/health/k1
```

## Instalación en docker swarm

Para instalar el servicio, es necesario realizar los siguientes pasos:

### Crear red

Para verificar que exista: 
```bash
docker network ls | grep hdi_fiseg_data_net
```

Para crear la red: 
```bash
docker network create -d overlay hdi_fiseg_data_net
```

### Construir imagen docker

```bash
docker build -t dockerhub:5000/hdi-fiseg-amis:$(egrep version_servicio pom.xml | awk -F">" '{print $2}' | awk -F"<" '{print $1}') -f docker/Dockerfile .
```
Donde:
* $(egrep version_servicio pom.xml | awk -F">" '{print $2}' | awk -F"<" '{print $1}'): Obtiene la versión que esta en el archivo **pom.xml**

### Correr docker container en docker swarm(docker-compose)
Substituir la definición del docker compose:

```bash
version: '3'

services:

  amis:
    image: dockerhub:6004/hdi-fiseg-amis:0.0.0
    restart: always
    ports:
      - 8091:8091
    environment:
      - SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1=jdbc:sqlserver://dev.interware.com.mx:1433;databaseName=fiseg
      - FISEG_DS_ORIGIN_USERNAME=sa
      - FISEG_DS_ORIGIN_PASSWORD=myPassword
      - FISEG_DS_ORIGIN_SCHEMA=dbo
      - FISEG_KAFKA_BOOTSTRAP_ADDRESS=dev.interware.com.mx:9092
      - FISEG_KAFKA_TRANSFORM_TOPIC=transform_topic
    networks:
      - hdi_fiseg_net
     
networks:
  hdi_fiseg_net:
    external:
        name: hdi_fiseg_data_net    
```
**Campos a actualizar:**

* **image**: Actualizar **dockerhub:5000** con el hostname y puerto del docker registry. También actualizar la **versión** que deseamos instalar
* **ports**: Si deseamos que nuestro servicio se exponga en otro puerto cambiar el puerto del lazo izquierdo
* **SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1**:URL de la base de datos SQL origen
* **FISEG_DS_ORIGIN_USERNAME**: Usuario de la base de datos SQL origen 
* **FISEG_DS_ORIGIN_PASSWORD**: Contraseña de la base de datos SQL origen
* **FISEG_DS_ORIGIN_SCHEMA**: Esquema de la base de datos SQL origen
* **FISEG_KAFKA_BOOTSTRAP_ADDRESS**: Hostname y puerto donde esta corriendo el broker de Kafka
* **FISEG_KAFKA_TRANSFORM_TOPIC**: Topico de Kafka donde se leen los mensajes

Ejecutar el siguiente comando:
```bash
docker stack deploy --prune --with-registry-auth --resolve-image always --compose-file=docker/docker-compose.yml hdi-fiseg-amis
```

### Correr docker container en docker swarm(docker service)

```bash
docker service create  \
--name hdi-fiseg-amis \
--env SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1=jdbc:sqlserver://dev.interware.com.mx:1433;databaseName=fiseg \
--env FISEG_DS_ORIGIN_USERNAME=sa \
--env FISEG_DS_ORIGIN_PASSWORD=myPassword \
--env FISEG_DS_ORIGIN_SCHEMA=dbo \
--env FISEG_KAFKA_BOOTSTRAP_ADDRESS=dev.interware.com.mx:9092 \
--env FISEG_KAFKA_TRANSFORM_TOPIC=transform_topic \
--publish 8091:8091 \
dockerhub:5000/hdi-fiseg-amis:0.0.0 

```
**Donde:**

 * **image**: **dockerhub:5000** es el hostname y puerto del docker registry. 
 * **0.0.0** Es la **versión** que deseamos instalar
 * **publish**: Si deseamos que nuestro servicio se exponga en otro puerto cambiar el puerto del lazo izquierdo
 * **SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1**:URL de la base de datos SQL origen
 * **FISEG_DS_ORIGIN_USERNAME**: Usuario de la base de datos SQL origen 
 * **FISEG_DS_ORIGIN_PASSWORD**: Contraseña de la base de datos SQL origen
 * **FISEG_DS_ORIGIN_SCHEMA**: Esquema de la base de datos SQL origen
 * **FISEG_KAFKA_BOOTSTRAP_ADDRESS**: Hostname y puerto donde esta corriendo el broker de Kafka
 * **FISEG_KAFKA_TRANSFORM_TOPIC**: Topico de Kafka donde se leen los mensajes
