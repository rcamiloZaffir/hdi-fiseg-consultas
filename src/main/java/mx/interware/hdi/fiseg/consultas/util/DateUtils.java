package mx.interware.hdi.fiseg.consultas.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Utileria para manejo de fechas dentro del microservicio
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class DateUtils {

	private static final String YM_DH_MS_PATTERN = "yyyy-MM-dd hh:mm:ss.SSS";

	private static final String PATTERN_DATE_AMIS = "EEE, dd MMM yyyy HH:mm:sss Z";

	private static final String PATTERN_DATE_STANDAR = "yyyy/dd/mm HH:mm:ss";

	/**
	 * Covierte un objeto String en formato "yyyy-MM-dd hh:mm:ss.SSS" a un {@link Date}
	 * 
	 * @param dateStr Objeto a convertir
	 * @return Objeto convertido
	 * @throws ParseException Por error en la transformaci&oacute;n
	 */
	public static Date parseYmdhms(String dateStr) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(YM_DH_MS_PATTERN);
		return format.parse(dateStr);
	}

	/**
	 * Covierte un objeto String en formato "EEE, dd MMM yyyy HH:mm:sss Z" enviado en headers AMIS para autenticaci&oacute;n a un {@link Date}
	 * 
	 * @param dateStr Objeto a convertir
	 * @return Objeto convertido
	 * @throws ParseException Por error en la transformaci&oacute;n
	 */
	public static Date parseDateAmis(String dateStr) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(PATTERN_DATE_AMIS, Locale.ENGLISH);
		return format.parse(dateStr);
	}

	/**
	 * Covierte Date a String en formato "yyyy/dd/mm HH:mm:ss" para visualizaci&oacute;n f&aacute;cil en logs
	 * 
	 * @param dateStr Objeto a convertir
	 * @return Objeto convertido
	 * @throws ParseException Por error en la transformaci&oacute;n
	 */
	public static String formatStandar(Date date) {
		SimpleDateFormat format = new SimpleDateFormat(PATTERN_DATE_STANDAR);
		return format.format(date);
	}

	/**
	 * Obtiene la fecha actual en formato "yyyy-MM-dd hh:mm:ss.SSS"
	 * 
	 * @return Cadena con el formato requerido
	 */
	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfts = new SimpleDateFormat(YM_DH_MS_PATTERN);
		return sdfts.format(new Date());
	}

	/**
	 * Constructor privado de la aplicaci&oacute;n
	 */
	private DateUtils() {
	}

}