package mx.interware.hdi.fiseg.consultas.exception;

/**
 * 
 * Excepci&oacute;n que encapsula el manejo de errores del microservicio
 * 
 * @author Interware
 * @since 2020-10-16
 */
public class AMISDispatchException extends RuntimeException {
	private static final long serialVersionUID = -8034440099558628040L;

	private final Integer status;
	private final String mensaje;

	/**
	 * Crea una excepci&oacute;n a partir de un mensaje y un estatus
	 * 
	 * @param message Mensaje del error
	 * @param status  Estado del error
	 */
	public AMISDispatchException(String message, Integer status) {
		super(message);
		this.mensaje = message;
		this.status = status;
	}

	/**
	 * Crea una excepci&oacute;n a partir de un mensaje y un objecto {@link Exception}
	 * 
	 * @param message Mensaje de la excepción
	 * @param ex      Objeto {@linkplain Exception}
	 */
	public AMISDispatchException(String message, Exception ex) {
		super(message, ex);
		this.mensaje = message;
		this.status = 500;
	}

	/**
	 * Crea una excepci&oacute;n a partir de un objecto {@link Exception}
	 * 
	 * @param ex Objeto {@linkplain Exception}
	 */
	public AMISDispatchException(Exception ex) {
		super(ex);
		this.mensaje = "Error al procesar la informacion";
		this.status = 500;
	}

	/**
	 * Retorn el estado de la excepci&oacute;n
	 * 
	 * @return estatus
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Retorn el mensaje de la excepci&oacute;n
	 * 
	 * @return Mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

}
