package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

/**
 * Clase que contiene una respuesta de Amis (Consulta por Poliza)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class ResponseAmisPolizaDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -5759561147707718267L;

	private boolean success;
	private PolizaDTO data;
	private ErrorAmisDTO error;

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the data
	 */
	public PolizaDTO getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(PolizaDTO data) {
		this.data = data;
	}

	/**
	 * @return the error
	 */
	public ErrorAmisDTO getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(ErrorAmisDTO error) {
		this.error = error;
	}


}