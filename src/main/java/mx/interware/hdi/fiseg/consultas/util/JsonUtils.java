package mx.interware.hdi.fiseg.consultas.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisPolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.TokenAmisDTO;
import mx.interware.hdi.fiseg.consultas.exception.AMISDispatchException;

/**
 * Utileria para el manejo y conversion de JSONs
 * 
 * @author Interware
 * @since 2020-10-17
 * 
 */
public class JsonUtils {
	private static final Logger LOGGER = LogManager.getLogger(JsonUtils.class);

	private static ObjectMapper mapper = new ObjectMapper();
	private static ObjectMapper amisMapper = new ObjectMapper();

	static {
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		mapper.disable(MapperFeature.AUTO_DETECT_CREATORS, MapperFeature.AUTO_DETECT_FIELDS,
				MapperFeature.AUTO_DETECT_GETTERS, MapperFeature.AUTO_DETECT_IS_GETTERS);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		amisMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		amisMapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
		amisMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		amisMapper.enable(SerializationFeature.INDENT_OUTPUT);
	}

	/**
	 * Constructor privado
	 */
	private JsonUtils() {
	}

	/**
	 * Convierte un objeto a un JSON Gen&eacute;rico V&aacute;lido
	 * 
	 * @param obj Objeto a covertir
	 * @return Json obtenido
	 */
	public static String toJson(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			LOGGER.error("Error al parsear a JSON", e);
			throw new AMISDispatchException(e);
		}
	}

	/**
	 * Convierte un objeto a un JSON V&aacute;lido para AMIS
	 * 
	 * @param obj Objeto a covertir
	 * @return Json obtenido
	 */
	public static String toAmisJson(Object obj) {
		try {
			return amisMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			LOGGER.error("Error al convertir elemento", e);
			throw new AMISDispatchException(e);
		}
	}

	/**
	 * Convierte un JSON a un Objeto {@link ResponseAmisDTO}
	 * 
	 * @param json JSON a convertir
	 * @return Objeto recuperado
	 */
	public static ResponseAmisDTO deserializeResponseAmis(String json) {
		try {
			LOGGER.trace("Response json: {}", json);
			ResponseAmisDTO response = amisMapper.readValue(json, ResponseAmisDTO.class);
			LOGGER.trace("response: {}", response);

			return response;
		} catch (Exception ex) {
			LOGGER.error("Error al procesar el reponse", ex);
			throw new AMISDispatchException(ex);
		}
	}

	/**
	 * Convierte un JSON a un Objeto {@link ResponseAmisPolizaDTO}
	 * 
	 * @param json JSON a convertir
	 * @return Objeto recuperado
	 */
	public static ResponseAmisPolizaDTO deserializeResponseAmisPoliza(String json) {
		try {
			LOGGER.trace("Response json: {}", json);
			ResponseAmisPolizaDTO response = amisMapper.readValue(json, ResponseAmisPolizaDTO.class);
			LOGGER.trace("response: {}", response);

			return response;
		} catch (Exception ex) {
			LOGGER.error("Error al procesar el reponse", ex);
			throw new AMISDispatchException(ex);
		}
	}

	/**
	 * Convierte un JSON a un Objeto {@link TokenAmisDTO}
	 * 
	 * @param json JSON a convertir
	 * @return Objeto recuperado
	 */
	public static TokenAmisDTO deserializeToken(String json) {
		try {
			LOGGER.trace("Token json: {}", json);
			return amisMapper.readValue(json, TokenAmisDTO.class);
		} catch (Exception ex) {
			LOGGER.error("Error al procesar el token", ex);
			throw new AMISDispatchException(ex);
		}
	}

}