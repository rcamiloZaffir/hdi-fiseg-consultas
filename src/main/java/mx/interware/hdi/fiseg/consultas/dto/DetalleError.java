package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

/**
 * Clase para encapsular la notificaci&oacute;n de errores propagados por el
 * sistema
 * 
 * @author Interware
 * @since 2020-10-14
 *
 */
public class DetalleError extends AmisDTO implements Serializable {

	private static final long serialVersionUID = -7189964640853606628L;

	private int status;

	private String message;

	private String errorTimestamp;

	private ErrorType errorType;

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the errorTimestamp
	 */
	public String getErrorTimestamp() {
		return errorTimestamp;
	}

	/**
	 * @param errorTimestamp the errorTimestamp to set
	 */
	public void setErrorTimestamp(String errorTimestamp) {
		this.errorTimestamp = errorTimestamp;
	}

	/**
	 * @return the errorType
	 */
	public ErrorType getErrorType() {
		return errorType;
	}

	/**
	 * @param errorType the errorType to set
	 */
	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}

	/**
	 * Enumeraci&oacute;n que representa el tipo de error WEB a propapar
	 */
	public enum ErrorType {

		HANDLED, UNHANDLED;
	}
}
