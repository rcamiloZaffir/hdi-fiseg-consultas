package mx.interware.hdi.fiseg.consultas.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;
import mx.interware.hdi.fiseg.consultas.dto.TipoBusqueda;
import mx.interware.hdi.fiseg.consultas.exception.ValidationFieldServiceException;
import mx.interware.hdi.fiseg.consultas.rest.RestCaller;
import mx.interware.hdi.fiseg.consultas.util.ConvertPolizaUtils;

/**
 * Clase que implementa las operaciones de busqueda de poliza
 * 
 * @author Interware
 * @since 2020-10-17
 */
@Service
public class ConsultasServiceImpl implements ConsultasService {
	private static final Logger LOGGER = LogManager.getLogger(ConsultasServiceImpl.class);

	@Autowired
	private RestCaller restCaller;

	private static final String PATTERN_VIN = "^\\w{17}$";
	
	private static final String PATTERN_POLIZA = "^[\\-\\w]{2,18}$|^[\\-\\d]{11,12}$|^\\d{9,10}$";
	private final Pattern patternPoliza;
	private final Pattern patternVin;

	@Autowired
	public ConsultasServiceImpl(RestCaller restCaller) {
		this.restCaller = restCaller;
		patternPoliza = Pattern.compile(PATTERN_POLIZA);
		patternVin = Pattern.compile(PATTERN_VIN);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PolizaFinalDTO getElementoAmisDTO(String idElementoAMIS) {
		LOGGER.info("Obteniendo consulta del elemento {} ", idElementoAMIS);
		 idElementoAMIS = idElementoAMIS.trim();
		LOGGER.info("Obteniendo consulta del elemento trim {} ", idElementoAMIS);

		TipoBusqueda tipoBusqueda = getTipoBusqueda(idElementoAMIS);

		if (TipoBusqueda.NOT_FOUND.equals(tipoBusqueda)) {
			throw new ValidationFieldServiceException(HttpStatus.BAD_REQUEST.value(), "Id Enviado", "No es valido");
		}

		LOGGER.info("Tipo de busqueda = {}", tipoBusqueda);

		String token = restCaller.getToken();
		PolizaDTO poliza = null;
		
		LOGGER.trace("Token generado => {}", token);
		if (TipoBusqueda.POLIZA.equals(tipoBusqueda)) {
			LOGGER.debug("Obteniendo elemento por Poliza");
			poliza = restCaller.getByPoliza(idElementoAMIS, token);
		} else {
			LOGGER.debug("Obteniendo elemento por Vin");
			poliza = restCaller.getByVin(idElementoAMIS, token);
		}

		return ConvertPolizaUtils.convert(poliza,tipoBusqueda);
	}

	/**
	 * Define el tipo de b&uacute;squeda a realizar de acuerdo a elemento enviado
	 * @param idElemento Con los datos para consulta
	 * @return {@link TipoBusqueda}
	 */
	private TipoBusqueda getTipoBusqueda(String idElemento) {
		Matcher m = patternPoliza.matcher(idElemento);
		if (m.find()) {
			LOGGER.debug("Elemento tipo POLIZA");
			return TipoBusqueda.POLIZA;
		} else {
			m = patternVin.matcher(idElemento);
			if (m.find()) {
				LOGGER.debug("Elemento tipo VIN");
				return TipoBusqueda.VIN;
			} else {
				LOGGER.error("Error al obtener el tipo de elemento");
				return TipoBusqueda.NOT_FOUND;
			}
		}
	}

}
