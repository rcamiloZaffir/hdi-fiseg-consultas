package mx.interware.hdi.fiseg.consultas.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Define todas las constantes utilizadas en el MS
 * 
 * @author Interware
 * @since 2020-10-17
 */
public class RestTemplateUtils {

    private static final Logger LOGGER = LogManager.getLogger(RestTemplateUtils.class);

    /**
     * Constructor privado
     */
    private RestTemplateUtils() {
    }

    /**
     * Crea un RestTemplate builder para inhabilitar la validaci&oacute;n del certificado. Esto se debe dado que HDI Firma el certificado de bluemix y esto hace
     * que parezca uno que no es de confianza Por lo tanto se pidió deshabilitarlo para no impedir la salida
     * 
     * @param restTemplateBuilder Objeto a setear las propiedades
     * @return Objeto con la configuraci&oacute;n
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    @SuppressWarnings("all")
    public static RestTemplate disableSSL(RestTemplateBuilder restTemplateBuilder) throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                LOGGER.info("Se omite validación por certificado HDI");
            }

            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                LOGGER.info("Se omite validación por certificado HDI");
            }
        } };
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
        HttpComponentsClientHttpRequestFactory customRequestFactory = new HttpComponentsClientHttpRequestFactory();
        customRequestFactory.setHttpClient(httpClient);

        RestTemplate restTemplateTemp = restTemplateBuilder.requestFactory(() -> customRequestFactory).build();
        return restTemplateTemp;

    }
}