package mx.interware.hdi.fiseg.consultas.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * Utileria para el manejo de las cadenas "Strings" dentro del MS
 * 
 * @author Interware
 * @since 2020-10-17
 * 
 */
public class StringUtils {

    private static final Logger LOGGER = LogManager.getLogger(StringUtils.class);

    private static final String LINES_IN_STRING = "\\\\n";
    private static final String LINES_NEW = "\n";

    @SuppressWarnings("all")
    public static String cleanCertificate(String certificate) {
        LOGGER.debug("Certificado=> {}", certificate);
        return certificate.replaceAll(LINES_IN_STRING, LINES_NEW);
    }

    private StringUtils() {
    }
}