package mx.interware.hdi.fiseg.consultas.dto;

import mx.interware.hdi.fiseg.consultas.util.JsonUtils;

/**
 * Clase abstracta con la definici&oacute;n del toString para todos los
 * elementos que la heredaran
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public abstract class AmisDTO {

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return JsonUtils.toAmisJson(this);

	}

}
