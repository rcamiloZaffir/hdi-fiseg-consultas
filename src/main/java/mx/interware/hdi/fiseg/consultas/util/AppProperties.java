package mx.interware.hdi.fiseg.consultas.util;

import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_INITVECTOR;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_ITERATIONS;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_KEYSIZE;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_PASSWORD;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_SALT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_LOG_LEVEL;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AMIS_CERTIFICATE;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AMIS_PRIVATE_KEY;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AUTHENTICATION_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_POLIZA_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_VIN_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_SSL_VALIDATION;
import static mx.interware.hdi.fiseg.consultas.util.Constants.TIMEZONE;

import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Componente para acceder a las variables de configuraci&oacute;n del microservicio
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@Component
@Scope("prototype")
public class AppProperties {
    private static final Logger LOGGER = LogManager.getLogger(AppProperties.class);

    private static Map<String, String> propertiesMap = new TreeMap<>();
    private static Map<String, String> env = System.getenv();

    @Value("${application.timezone}")
    private String timeZone;

    @Value("${rest.amis.endpoints.authentication}")
    private String restAuthenticationEndpoint;

    @Value("${rest.amis.endpoints.poliza}")
    private String restPolizaEndpoint;

    @Value("${rest.amis.endpoints.vin}")
    private String restVinEndpoint;

    @Value("${rest.amis.certificate}")
    private String restCertificate;

    @Value("${rest.amis.private_key}")
    private String restPrivateKey;

    @Value("${service.default.log_level}")
    private String logLevel;

    @Value("${service.enable.ssl_validation}")
    private String enableSSLValidation;

    @Value("${service.crypt.pasd}")
    private String cryptPassd;

    @Value("${service.crypt.salt}")
    private String cryptSalt;

    @Value("${service.crypt.iterations}")
    private String cryptIterations;

    @Value("${service.crypt.init_vector}")
    private String cryptInitVector;

    @Value("${service.crypt.key_size}")
    private String cryptKeySize;

    /**
     * Agrega una propiedad a la configurac&oacute;n
     * 
     * @param name         Nombre de la propiedad
     * @param defaultValue Valor de la propriedad
     */
    private void addProperty(String name, String defaultValue) {
        String value = env.get(name);
        if (value != null) {
            LOGGER.info("-> Adding application property from environment variable :{}=\"{}\"] ...", name, value);
            propertiesMap.put(name, value);
        } else {
            LOGGER.info("-> Adding application property from default configuration:{}=\"{}\"] ...", name, value);
            propertiesMap.put(name, defaultValue);
        }
    }

    /**
     * Inicializa las propiedades de la aplicaci&oacute;n
     */
    @PostConstruct
    private void init() {
        addProperty(TIMEZONE, timeZone);
        addProperty(FISEG_REST_AUTHENTICATION_ENDPOINT, restAuthenticationEndpoint);
        addProperty(FISEG_REST_POLIZA_ENDPOINT, restPolizaEndpoint);
        addProperty(FISEG_REST_VIN_ENDPOINT, restVinEndpoint);
        addProperty(FISEG_REST_AMIS_CERTIFICATE, restCertificate);
        addProperty(FISEG_REST_AMIS_PRIVATE_KEY, restPrivateKey);
        addProperty(FISEG_SSL_VALIDATION, enableSSLValidation);
        addProperty(FISEG_LOG_LEVEL, logLevel);
        addProperty(CRYPT_PASSWORD, cryptPassd);
        addProperty(CRYPT_SALT, cryptSalt);
        addProperty(CRYPT_ITERATIONS, cryptIterations);
        addProperty(CRYPT_INITVECTOR, cryptInitVector);
        addProperty(CRYPT_KEYSIZE, cryptKeySize);
    }

    /**
     * Obtiene el valor de una propiedad
     * 
     * @param name Nombre de la propiedad
     * @return Valor obtenido
     */
    public String getProperty(String name) {
        String property = propertiesMap.get(name);
        LOGGER.debug("Getting property:[{}=\"{}\"] ...", name, property);
        return property;
    }
}