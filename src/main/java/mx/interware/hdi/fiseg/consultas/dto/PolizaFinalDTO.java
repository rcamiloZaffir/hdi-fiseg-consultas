package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de una Poliza valida para el FRONT (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Objeto con la infomación de la poliza")
public class PolizaFinalDTO extends AmisDTO implements Serializable {
    private static final long serialVersionUID = 2143683712872744094L;

    @ApiModelProperty(notes = "Identificador del agente")
    @JsonProperty("agent")
    private String agenteFinal;

    @ApiModelProperty(notes = "Tipo documento")
    @JsonProperty("doctype")
    private String doctypeFinal;

    @ApiModelProperty(notes = "Número de poliza")
    @JsonProperty("policyId")
    private String numeroPolizaFinal;

    @ApiModelProperty(notes = "Numero de referencia")
    @JsonProperty("referenceNumber")
    private String numeroReferenciaFinal;

    @ApiModelProperty(notes = "Canal de venta")
    @JsonProperty("channelSale")
    private Integer canalVentaFinal;

    @ApiModelProperty(notes = "Fecha de emision de Poliza")
    @JsonProperty("issueDate")
    private String fechaEmisionFinal;

    @ApiModelProperty(notes = "Fecha inicio vigencia de la Poliza")
    @JsonProperty("initDate")
    private String fechaInicioVigenciaFinal;

    @ApiModelProperty(notes = "Fecha fin vigencia de la Poliza")
    @JsonProperty("endDate")
    private String fechaFinalVigenciaFinal;

    @ApiModelProperty(notes = "Tipo Movimiento")
    @JsonProperty("moveType")
    private String tipoMovimientoFinal;

    @ApiModelProperty(notes = "Lista de vehiculos")
    @JsonProperty("vehicles")
    private List<VehiculoAmisDTO> vehicles;

    @ApiModelProperty(notes = "Lista de cobertura")
    @JsonProperty("coverage")
    private List<CoberturaFinalDTO> coverage;

    @ApiModelProperty(notes = "Lista de clientes")
    @JsonProperty("clients")
    private List<ClienteAMISDTO> clientes;

    @ApiModelProperty(notes = "Lista de beneficiarios")

    @JsonProperty("beneficiaries")
    private List<BeneficiarioAmisDTO> beneficiarios;

    @ApiModelProperty(notes = "Tipo Búsqueda realizada")
    private String tipoBusquedaFinal;

    /**
     * @return the agenteFinal
     */
    public String getAgenteFinal() {
        return agenteFinal;
    }

    /**
     * @param agenteFinal the agenteFinal to set
     */
    public void setAgenteFinal(String agenteFinal) {
        this.agenteFinal = agenteFinal;
    }

    /**
     * @return the doctypeFinal
     */
    public String getDoctypeFinal() {
        return doctypeFinal;
    }

    /**
     * @param doctypeFinal the doctypeFinal to set
     */
    public void setDoctypeFinal(String doctypeFinal) {
        this.doctypeFinal = doctypeFinal;
    }

    /**
     * @return the numeroPolizaFinal
     */
    public String getNumeroPolizaFinal() {
        return numeroPolizaFinal;
    }

    /**
     * @param numeroPolizaFinal the numeroPolizaFinal to set
     */
    public void setNumeroPolizaFinal(String numeroPolizaFinal) {
        this.numeroPolizaFinal = numeroPolizaFinal;
    }

    /**
     * @return the numeroReferenciaFinal
     */
    public String getNumeroReferenciaFinal() {
        return numeroReferenciaFinal;
    }

    /**
     * @param numeroReferenciaFinal the numeroReferenciaFinal to set
     */
    public void setNumeroReferenciaFinal(String numeroReferenciaFinal) {
        this.numeroReferenciaFinal = numeroReferenciaFinal;
    }

    /**
     * @return the canalVentaFinal
     */
    public Integer getCanalVentaFinal() {
        return canalVentaFinal;
    }

    /**
     * @param canalVentaFinal the canalVentaFinal to set
     */
    public void setCanalVentaFinal(Integer canalVentaFinal) {
        this.canalVentaFinal = canalVentaFinal;
    }

    /**
     * @return the vehicles
     */
    public List<VehiculoAmisDTO> getVehicles() {
        return vehicles;
    }

    /**
     * @param vehicles the vehicles to set
     */
    public void setVehicles(List<VehiculoAmisDTO> vehicles) {
        this.vehicles = vehicles;
    }

    /**
     * @return the coverage
     */
    public List<CoberturaFinalDTO> getCoverage() {
        return coverage;
    }

    /**
     * @param coverage the coverage to set
     */
    public void setCoverage(List<CoberturaFinalDTO> coverage) {
        this.coverage = coverage;
    }

    /**
     * @return the clientes
     */
    public List<ClienteAMISDTO> getClientes() {
        return clientes;
    }

    /**
     * @param clientes the clientes to set
     */
    public void setClientes(List<ClienteAMISDTO> clientes) {
        this.clientes = clientes;
    }

    /**
     * @return the beneficiarios
     */
    public List<BeneficiarioAmisDTO> getBeneficiarios() {
        return beneficiarios;
    }

    /**
     * @param beneficiarios the beneficiarios to set
     */
    public void setBeneficiarios(List<BeneficiarioAmisDTO> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    /**
     * @return the tipoBusquedaFinal
     */
    public String getTipoBusquedaFinal() {
        return tipoBusquedaFinal;
    }

    /**
     * @param tipoBusquedaFinal the tipoBusquedaFinal to set
     */
    public void setTipoBusquedaFinal(String tipoBusquedaFinal) {
        this.tipoBusquedaFinal = tipoBusquedaFinal;
    }

    /**
     * @return the fechaEmisionFinal
     */
    public String getFechaEmisionFinal() {
        return fechaEmisionFinal;
    }

    /**
     * @param fechaEmisionFinal the fechaEmisionFinal to set
     */
    public void setFechaEmisionFinal(String fechaEmisionFinal) {
        this.fechaEmisionFinal = fechaEmisionFinal;
    }

    /**
     * @return the fechaInicioVigenciaFinal
     */
    public String getFechaInicioVigenciaFinal() {
        return fechaInicioVigenciaFinal;
    }

    /**
     * @param fechaInicioVigenciaFinal the fechaInicioVigenciaFinal to set
     */
    public void setFechaInicioVigenciaFinal(String fechaInicioVigenciaFinal) {
        this.fechaInicioVigenciaFinal = fechaInicioVigenciaFinal;
    }

    /**
     * @return the fechaFinalVigenciaFinal
     */
    public String getFechaFinalVigenciaFinal() {
        return fechaFinalVigenciaFinal;
    }

    /**
     * @param fechaFinalVigenciaFinal the fechaFinalVigenciaFinal to set
     */
    public void setFechaFinalVigenciaFinal(String fechaFinalVigenciaFinal) {
        this.fechaFinalVigenciaFinal = fechaFinalVigenciaFinal;
    }

    /**
     * @return the tipoMovimientoFinal
     */
    public String getTipoMovimientoFinal() {
        return tipoMovimientoFinal;
    }

    /**
     * @param tipoMovimientoFinal the tipoMovimientoFinal to set
     */
    public void setTipoMovimientoFinal(String tipoMovimientoFinal) {
        this.tipoMovimientoFinal = tipoMovimientoFinal;
    }

}