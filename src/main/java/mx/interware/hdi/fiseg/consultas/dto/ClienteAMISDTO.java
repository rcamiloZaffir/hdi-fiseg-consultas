package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de un Cliente (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "Objeto con la infomación del cliente")
public class ClienteAMISDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -8455180957618285713L;

	@ApiModelProperty(notes = "Apellido paterno del cliente")
	@JsonProperty("apPaterno")
	private String apPaternoCliente;

	@ApiModelProperty(notes = "Apellido materno del cliente")
	@JsonProperty("apMaterno")
	private String apMaternoCliente;

	@ApiModelProperty(notes = "Nombre del cliente")
	@JsonProperty("nombre")
	private String nombreCliente;

	@ApiModelProperty(notes = "RFC del cliente")
	@JsonProperty("RFC")
	private String rfcCliente;

	@ApiModelProperty(notes = "CURP del cliente")
	@JsonProperty("CURP")
	private String curpCliente;

	/**
	 * @return the apPaternoCliente
	 */
	public String getApPaternoCliente() {
		return apPaternoCliente;
	}

	/**
	 * @param apPaternoCliente the apPaternoCliente to set
	 */
	public void setApPaternoCliente(String apPaternoCliente) {
		this.apPaternoCliente = apPaternoCliente;
	}

	/**
	 * @return the apMaternoCliente
	 */
	public String getApMaternoCliente() {
		return apMaternoCliente;
	}

	/**
	 * @param apMaternoCliente the apMaternoCliente to set
	 */
	public void setApMaternoCliente(String apMaternoCliente) {
		this.apMaternoCliente = apMaternoCliente;
	}

	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/**
	 * @return the rfcCliente
	 */
	public String getRfcCliente() {
		return rfcCliente;
	}

	/**
	 * @param rfcCliente the rfcCliente to set
	 */
	public void setRfcCliente(String rfcCliente) {
		this.rfcCliente = rfcCliente;
	}

	/**
	 * @return the curpCliente
	 */
	public String getCurpCliente() {
		return curpCliente;
	}

	/**
	 * @param curpCliente the curpCliente to set
	 */
	public void setCurpCliente(String curpCliente) {
		this.curpCliente = curpCliente;
	}

}