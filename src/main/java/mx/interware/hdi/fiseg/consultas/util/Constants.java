package mx.interware.hdi.fiseg.consultas.util;

/**
 * Define todas las constantes utilizadas en el MS
 * 
 * @author Interware
 * @since 2020-10-17
 */
public class Constants {
	public static final String TIMEZONE = "FISEG_TIMEZONE";

	public static final String FISEG_REST_AUTHENTICATION_ENDPOINT = "FISEG_REST_AUTHENTICATION_ENDPOINT";
	public static final String FISEG_REST_POLIZA_ENDPOINT = "FISEG_REST_POLIZA_ENDPOINT";
    public static final String FISEG_REST_VIN_ENDPOINT = "FISEG_REST_VIN_ENDPOINT";
    public static final String FISEG_REST_AMIS_CERTIFICATE = "FISEG_REST_AMIS_CERTIFICATE";
    public static final String FISEG_REST_AMIS_PRIVATE_KEY = "FISEG_REST_AMIS_PRIVATE_KEY";
    public static final String FISEG_SSL_VALIDATION = "FISEG_SSL_VALIDATION";
    public static final String FISEG_LOG_LEVEL = "FISEG_LOG_LEVEL";


    public static final String CRYPT_PASSWORD = "CRYPT_PASSWORD";
    public static final String CRYPT_SALT = "CRYPT_SALT";
    public static final String CRYPT_ITERATIONS = "CRYPT_ITERATIONS";
    public static final String CRYPT_INITVECTOR = "CRYPT_INITVECTOR";
    public static final String CRYPT_KEYSIZE = "CRYPT_KEYSIZE";

	public static final String SUCCESS = "success";
	public static final String WAITING = "waiting";
	public static final String ERROR = "error";

	/**
	 * Constructor privado
	 */
	private Constants() {
	}
}