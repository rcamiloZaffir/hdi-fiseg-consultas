package mx.interware.hdi.fiseg.consultas.service;

import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;

/**
 * Clase que encapsula las operaciones de busqueda de poliza
 * 
 * @author Interware
 * @since 2020-10-17
 */
public interface ConsultasService {

	/**
	 * Busca una poliza en los servicios expuestos de AMIS.
	 * 
	 * @param idElementoAMIS Numero de poliza o VIN
	 * @return Poliza encontrada
	 */
	PolizaFinalDTO getElementoAmisDTO(String idElementoAMIS);
}