package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de un Vehiculo  (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "Objeto con la infomación del vehiculo")
public class VehiculoAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -1025002607320496195L;

	@ApiModelProperty(notes = "Uso del vehiculo")
	private Integer uso;

	@ApiModelProperty(notes = "Vehicle Identification Number")
	private String vin;
	@ApiModelProperty(notes = "Modelo del vehículo")
	private Integer modelo;
	@ApiModelProperty(notes = "Placa del vehículo")
	private String placa;
	@ApiModelProperty(notes = "Inciso")
	private String inciso;
	@ApiModelProperty(notes = "Marca tipo vehículo")
	private Integer marcaTipo;
	@ApiModelProperty(notes = "Inciso Estatus")
	private Integer incisoEstatus;
	@ApiModelProperty(notes = "Tipo movimiento vehículo")
	private Integer tipoMovimiento;

	@ApiModelProperty(notes = "Tipo de seguro")
	private Short tipoSeguro;

	private List<CoberturaAmisDTO> coberturas;

	/**
	 * @return the uso
	 */
	public Integer getUso() {
		return uso;
	}

	/**
	 * @param uso the uso to set
	 */
	public void setUso(Integer uso) {
		this.uso = uso;
	}

	/**
	 * @return the vin
	 */
	public String getVin() {
		return vin;
	}

	/**
	 * @param vin the vin to set
	 */
	public void setVin(String vin) {
		this.vin = vin;
	}

	/**
	 * @return the modelo
	 */
	public Integer getModelo() {
		return modelo;
	}

	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	/**
	 * @return the inciso
	 */
	public String getInciso() {
		return inciso;
	}

	/**
	 * @param inciso the inciso to set
	 */
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}

	/**
	 * @return the marcaTipo
	 */
	public Integer getMarcaTipo() {
		return marcaTipo;
	}

	/**
	 * @param marcaTipo the marcaTipo to set
	 */
	public void setMarcaTipo(Integer marcaTipo) {
		this.marcaTipo = marcaTipo;
	}

	/**
	 * @return the incisoEstatus
	 */
	public Integer getIncisoEstatus() {
		return incisoEstatus;
	}

	/**
	 * @param incisoEstatus the incisoEstatus to set
	 */
	public void setIncisoEstatus(Integer incisoEstatus) {
		this.incisoEstatus = incisoEstatus;
	}

	/**
	 * @return the tipoMovimiento
	 */
	public Integer getTipoMovimiento() {
		return tipoMovimiento;
	}

	/**
	 * @param tipoMovimiento the tipoMovimiento to set
	 */
	public void setTipoMovimiento(Integer tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	/**
	 * @return the tipoSeguro
	 */
	public Short getTipoSeguro() {
		return tipoSeguro;
	}

	/**
	 * @param tipoSeguro the tipoSeguro to set
	 */
	public void setTipoSeguro(Short tipoSeguro) {
		this.tipoSeguro = tipoSeguro;
	}

	/**
	 * @return the coberturas
	 */
	public List<CoberturaAmisDTO> getCoberturas() {
		return coberturas;
	}

	/**
	 * @param coberturas the coberturas to set
	 */
	public void setCoberturas(List<CoberturaAmisDTO> coberturas) {
		this.coberturas = coberturas;
	}

}