package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

/**
 * Clase para mapeo de los errores que pudieran ocurrir en una petici&oacute;n a
 * los servicios AMIS
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class ErrorAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -6604324779308124976L;

	private Integer status;
	private String message;

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}