package mx.interware.hdi.fiseg.consultas.dto;

/**
 * Tipos de b&uacute;squeda posibles en la aplicaci&oacute;n
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public enum TipoBusqueda {
	VIN, POLIZA, NOT_FOUND;
}
