package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO con la lista de polizas enviadas en el response de AMIS
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class DataResponseAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -5759561147707718267L;

	@JsonProperty("Key")
	private String key;

	@JsonProperty("Record")
	private PolizaDTO record;

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the record
	 */
	public PolizaDTO getRecord() {
		return record;
	}

	/**
	 * @param record the record to set
	 */
	public void setRecord(PolizaDTO record) {
		this.record = record;
	}

}