package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de una poliza (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "Objeto con la infomación de la poliza de AMIS")
public class PolizaDTO extends AmisDTO implements Serializable {
    private static final long serialVersionUID = 2143683712872744094L;

    @ApiModelProperty(notes = "Identificador del agente")
    private String agente;

    @ApiModelProperty(notes = "Tipo documento")
    private String doctype;

    @ApiModelProperty(notes = "Número de poliza")
    private String numeroPoliza;

    @ApiModelProperty(notes = "Numero de referencia")
    private String numeroReferencia;

    @ApiModelProperty(notes = "Fecha de emisión de la Poliza")
    private String fechaEmision;

    @ApiModelProperty(notes = "Fecha Final de vigencia")
    private String fechaFinalVigencia;

    @ApiModelProperty(notes = "Fecha inicio de vigencia")
    private String fechaInicioVigencia;

    @ApiModelProperty(notes = "Tipo de movimiento")
    private String tipoMovimiento;

    @ApiModelProperty(notes = "Canal de venta")
    private Integer canalVenta;

    @ApiModelProperty(notes = "Lista de vehiculos")
    private List<VehiculoAmisDTO> vehiculos;

    @ApiModelProperty(notes = "Lista de cobertura")

    private List<CoberturaAmisDTO> coberturas;

    @ApiModelProperty(notes = "Lista de clientes")
    private List<ClienteAMISDTO> clientes;

    @ApiModelProperty(notes = "Lista de beneficiarios")
    private List<BeneficiarioAmisDTO> beneficiarios;

    /**
     * @return the agente
     */
    public String getAgente() {
        return agente;
    }

    /**
     * @param agente the agente to set
     */
    public void setAgente(String agente) {
        this.agente = agente;
    }

    /**
     * @return the doctype
     */
    public String getDoctype() {
        return doctype;
    }

    /**
     * @param doctype the doctype to set
     */
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    /**
     * @return the numeroPoliza
     */
    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    /**
     * @param numeroPoliza the numeroPoliza to set
     */
    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    /**
     * @return the numeroReferencia
     */
    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    /**
     * @param numeroReferencia the numeroReferencia to set
     */
    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    /**
     * @return the canalVenta
     */
    public Integer getCanalVenta() {
        return canalVenta;
    }

    /**
     * @param canalVenta the canalVenta to set
     */
    public void setCanalVenta(Integer canalVenta) {
        this.canalVenta = canalVenta;
    }

    /**
     * @return the vehiculos
     */
    public List<VehiculoAmisDTO> getVehiculos() {
        return vehiculos;
    }

    /**
     * @param vehiculos the vehiculos to set
     */
    public void setVehiculos(List<VehiculoAmisDTO> vehiculos) {
        this.vehiculos = vehiculos;
    }

    /**
     * @return the coberturas
     */
    public List<CoberturaAmisDTO> getCoberturas() {
        return coberturas;
    }

    /**
     * @param coberturas the coberturas to set
     */
    public void setCoberturas(List<CoberturaAmisDTO> coberturas) {
        this.coberturas = coberturas;
    }

    /**
     * @return the clientes
     */
    public List<ClienteAMISDTO> getClientes() {
        return clientes;
    }

    /**
     * @param clientes the clientes to set
     */
    public void setClientes(List<ClienteAMISDTO> clientes) {
        this.clientes = clientes;
    }

    /**
     * @return the beneficiarios
     */
    public List<BeneficiarioAmisDTO> getBeneficiarios() {
        return beneficiarios;
    }

    /**
     * @param beneficiarios the beneficiarios to set
     */
    public void setBeneficiarios(List<BeneficiarioAmisDTO> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    /**
     * @return the fechaEmision
     */
    public String getFechaEmision() {
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the fechaFinalVigencia
     */
    public String getFechaFinalVigencia() {
        return fechaFinalVigencia;
    }

    /**
     * @param fechaFinalVigencia the fechaFinalVigencia to set
     */
    public void setFechaFinalVigencia(String fechaFinalVigencia) {
        this.fechaFinalVigencia = fechaFinalVigencia;
    }

    /**
     * @return the fechaInicioVigencia
     */
    public String getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    /**
     * @param fechaInicioVigencia the fechaInicioVigencia to set
     */
    public void setFechaInicioVigencia(String fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    /**
     * @return the tipoMovimiento
     */
    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    /**
     * @param tipoMovimiento the tipoMovimiento to set
     */
    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

}