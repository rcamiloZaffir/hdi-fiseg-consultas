package mx.interware.hdi.fiseg.consultas.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.interware.hdi.fiseg.consultas.dto.CoberturaAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.CoberturaFinalDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;
import mx.interware.hdi.fiseg.consultas.dto.TipoBusqueda;

/**
 * Utileria para la conversi&oacute;n del Objeto AMIS a uno valido para los MS de consulta
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class ConvertPolizaUtils {

    private static final Logger LOGGER = LogManager.getLogger(ConvertPolizaUtils.class);

    private ConvertPolizaUtils() {
    }

    /**
     * Covierte un objeto {@link PolizaDTO} a {@link PolizaFinalDTO}
     * 
     * @param poliza Objeto a convertir
     * @return Objeto convertido
     */
    public static PolizaFinalDTO convert(PolizaDTO poliza, TipoBusqueda tipoBusqueda) {

        LOGGER.info("Iniciando conversión de poliza encontrada");
        PolizaFinalDTO result = new PolizaFinalDTO();
        result.setAgenteFinal(poliza.getAgente());
        result.setDoctypeFinal(poliza.getDoctype());
        result.setNumeroPolizaFinal(poliza.getNumeroPoliza());
        result.setNumeroReferenciaFinal(poliza.getNumeroReferencia());
        result.setCanalVentaFinal(poliza.getCanalVenta());
        result.setClientes(poliza.getClientes());
        result.setBeneficiarios(poliza.getBeneficiarios());
        result.setVehicles(poliza.getVehiculos());
        result.setTipoBusquedaFinal(tipoBusqueda.toString());
        result.setTipoMovimientoFinal(poliza.getTipoMovimiento());
        result.setFechaEmisionFinal(getFechaPortal(poliza.getFechaEmision()));
        result.setFechaFinalVigenciaFinal(getFechaPortal(poliza.getFechaFinalVigencia()));
        result.setFechaInicioVigenciaFinal(getFechaPortal(poliza.getFechaInicioVigencia()));
        if (poliza.getCoberturas() != null && !poliza.getCoberturas().isEmpty()) {
            List<CoberturaFinalDTO> coverage = new ArrayList<>();
            for (CoberturaAmisDTO c : poliza.getCoberturas()) {
                coverage.add(getCoberturaFinal(c));
            }
            result.setCoverage(coverage);
        }
        return result;
    }

    /**
     * Covierte un objeto {@link CoberturaAmisDTO} a {@link CoberturaFinalDTO}
     * 
     * @param dto Objeto a convertir
     * @return Objeto convertido
     */
    private static CoberturaFinalDTO getCoberturaFinal(CoberturaAmisDTO dto) {
        CoberturaFinalDTO result = new CoberturaFinalDTO();
        result.setIdCoverage(dto.getId());
        result.setInsuranceSum(dto.getSumaAsegurada());
        result.setStatus(dto.getEstatus());
        result.setInsuranceSumId(dto.getIdSumaAsegurada());
        return result;
    }

    public static final String MESSAGE_DATE = "FechaNoValida";

    private static String getFechaPortal(String fechaAMIS) {
        if (fechaAMIS == null) {
            return MESSAGE_DATE;
        }
        String[] campos = fechaAMIS.split("/");
        StringBuilder sb = new StringBuilder();
        if (campos.length == 3) {
            sb.append(campos[2]);
            sb.append(campos[1]);
            sb.append(campos[0]);
        } else {
            return MESSAGE_DATE;
        }
        return sb.toString();
    }
}