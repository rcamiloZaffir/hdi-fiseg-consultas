package mx.interware.hdi.fiseg.consultas.rest;

import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AMIS_CERTIFICATE;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AMIS_PRIVATE_KEY;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AUTHENTICATION_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_POLIZA_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_VIN_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_SSL_VALIDATION;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.CheckForNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import mx.interware.hdi.fiseg.consultas.dto.DataResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisPolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.TokenAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.TokenRequestAmisDTO;
import mx.interware.hdi.fiseg.consultas.exception.AMISDispatchException;
import mx.interware.hdi.fiseg.consultas.exception.NotFoundException;
import mx.interware.hdi.fiseg.consultas.util.AppProperties;
import mx.interware.hdi.fiseg.consultas.util.DateUtils;
import mx.interware.hdi.fiseg.consultas.util.DecryptStringUtils;
import mx.interware.hdi.fiseg.consultas.util.JsonUtils;
import mx.interware.hdi.fiseg.consultas.util.RestTemplateUtils;
import mx.interware.hdi.fiseg.consultas.util.StringUtils;

/**
 * Clase que encapsula las operaciones para las peticiones a la AMIS
 * 
 * @author Interware
 * @since 2020-10-17
 */
@Component
public class RestCaller {

    private static final String CAMPO_DATE = "Date";

    private static final long ONE_MINUTE_IN_MILLIS = 60000; // milisegundos por minuto

    private static final Logger LOGGER = LogManager.getLogger(RestCaller.class);

    private String authenticationToken;

    private final RestTemplate restTemplate;

    @Autowired
    private AppProperties appProperties;

    private Date fechaExpiracion;

    @Autowired
    private DecryptStringUtils decryptStringUtils;

    private String privateKey;

    /**
     * Crea una nueva instancia de la clase
     * 
     * @param restTemplateBuilder Objeto para creaci&oacute;n de peticiones REST
     * @param appProperties       Elemento con las propriedades y configuraciones
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    @Autowired
    public RestCaller(RestTemplateBuilder restTemplateBuilder, AppProperties appProperties) throws KeyManagementException, NoSuchAlgorithmException {
        if (appProperties.getProperty(FISEG_SSL_VALIDATION).equals("true")) {
            LOGGER.info("Creando el rest Template con validación SSL");
            this.restTemplate = restTemplateBuilder.build();
        } else {
            LOGGER.info("Creando el rest Template Omitiendo validación SSL !!!!!!!!!");
            this.restTemplate = RestTemplateUtils.disableSSL(restTemplateBuilder);
        }
        this.appProperties = appProperties;
    }

    /**
     * Retorna un token de autenticaci&oacute;n v&aacute;lido para AMIS
     * 
     * @return Token generado
     */
    public String getToken() {
        LOGGER.info("Getting authentication token ...");

        Date fechaActual = new Date();

        Boolean compDates = fechaExpiracion != null && (fechaActual.getTime() - fechaExpiracion.getTime()) > 0;

        LOGGER.debug("Resultado comp Dates= {}", compDates);

        if (authenticationToken == null || compDates) {

            LOGGER.info("Calling authentication service ...");
            String url = appProperties.getProperty(FISEG_REST_AUTHENTICATION_ENDPOINT);

            String certificate = appProperties.getProperty(FISEG_REST_AMIS_CERTIFICATE);
            initPrivateKey();
            certificate = StringUtils.cleanCertificate(certificate);
            TokenRequestAmisDTO tokenRequest = new TokenRequestAmisDTO();
            tokenRequest.setCertificate(certificate);
            tokenRequest.setPrivateKey(privateKey);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            try {

                LOGGER.info("url=> {}", url);
                LOGGER.trace("Request=> {}", tokenRequest);

                HttpEntity<TokenRequestAmisDTO> request = new HttpEntity<>(tokenRequest, headers);
                ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

                String tokenJson = response.getBody();

                LOGGER.trace("TOKEN => {}", tokenJson);

                TokenAmisDTO tokenAmis = JsonUtils.deserializeToken(tokenJson);

                authenticationToken = tokenAmis.getToken();
                setFechaExpiracion(response);
            } catch (Exception e) {
                LOGGER.error("Error al obtener el token ", e);
            }
        }

        LOGGER.debug("TOKEN => {}", authenticationToken);
        return authenticationToken;
    }

    private void setFechaExpiracion(ResponseEntity<String> response) {
        HttpHeaders headers = response.getHeaders();
        Date fechaTemp;
        try {
            List<String> listaHeaders = headers.get(CAMPO_DATE);
            if (listaHeaders != null && !listaHeaders.isEmpty()) {
                String dateAmis = listaHeaders.get(0);
                LOGGER.info("TOKEN_EXPIRACION {}", dateAmis);
                fechaTemp = DateUtils.parseDateAmis(dateAmis);

                fechaExpiracion = new Date(fechaTemp.getTime() + (10 * ONE_MINUTE_IN_MILLIS));

                LOGGER.debug("Se toma fecha del token");
            } else {
                setFechaDefault();
            }
        } catch (ParseException e) {
            LOGGER.error("No se pudo obtener una fecha de expiración de Token", e);
            setFechaDefault();
        }

        LOGGER.info("Se toma fecha de expiracion = {}", fechaExpiracion != null ? DateUtils.formatStandar(fechaExpiracion) : "-");
    }

    /**
     * Setea una fecha default para expiraci&oacute;n de toquen
     */
    private void setFechaDefault() {
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        fechaExpiracion = new Date(t + (10 * ONE_MINUTE_IN_MILLIS));
    }

    /**
     * Consulta el endpoint expuesto para obtener los datos de una poliza mendiante el numero de 'poliza'
     * 
     * @param poliza Numero de poliza
     * @param token  Token de autenticaci&oacute;n
     * @return Poliza encontrada
     */
    @CheckForNull
    public PolizaDTO getByPoliza(String poliza, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("access-token", token);
        StringBuilder polizaEndpoint = new StringBuilder(appProperties.getProperty(FISEG_REST_POLIZA_ENDPOINT));
        polizaEndpoint.append(poliza);
        LOGGER.info("URL Poliza => {}", polizaEndpoint);
        HttpEntity<String> request = new HttpEntity<>(headers);
        try {
            LOGGER.trace("Request {}", request);

            ResponseEntity<ResponseAmisPolizaDTO> response = restTemplate.exchange(polizaEndpoint.toString(), HttpMethod.GET, request,
                    ResponseAmisPolizaDTO.class);

            LOGGER.debug("Respuesta pol=> {} ", response.getStatusCode());

            return getPolizas(response);

        } catch (HttpClientErrorException.BadRequest e) {
            LOGGER.error("Se obtuvo el error {}", e.getResponseBodyAsString());
            ResponseAmisPolizaDTO response = JsonUtils.deserializeResponseAmisPoliza(e.getResponseBodyAsString());
            throw new NotFoundException(HttpStatus.NO_CONTENT.value(), response.getError().getMessage());
        } catch (HttpClientErrorException | NullPointerException e) {
            LOGGER.error("Error al obtener la poliza ", e);
            throw new AMISDispatchException("Error al conectar con el servicio de AMIS", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    /**
     * Consulta el endpoint expuesto para obtener los datos de una poliza mendiante el VIN del vehiculo
     * 
     * @param vin   VIN del vehiculo
     * @param token Token de autenticaci&oacute;n
     * @return Poliza encontrada
     */
    @CheckForNull
    public PolizaDTO getByVin(String vin, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("access-token", token);
        StringBuilder vinEndpoint = new StringBuilder(appProperties.getProperty(FISEG_REST_VIN_ENDPOINT));
        vinEndpoint.append(vin);
        HttpEntity<String> request = new HttpEntity<>(headers);

        LOGGER.info("URL Vin => {}", vinEndpoint);
        try {
            ResponseEntity<ResponseAmisDTO> response = restTemplate.exchange(vinEndpoint.toString(), HttpMethod.GET, request, ResponseAmisDTO.class);
            LOGGER.debug("Respuesta pol=> {} ", response.getStatusCode());
            List<DataResponseAmisDTO> data = null;

            data = getData(response);

            if (response.getStatusCode().equals(HttpStatus.OK) && data != null && !data.isEmpty()) {
                return data.get(0).getRecord();
            } else {
                LOGGER.debug("Error HTTP diferente 200");
                throw new NotFoundException(HttpStatus.NO_CONTENT.value(), "No se encontró elemento");
            }
        } catch (HttpClientErrorException.BadRequest e) {
            LOGGER.error("Error Obtenido={}", e.getResponseBodyAsString());
            ResponseAmisDTO response = JsonUtils.deserializeResponseAmis(e.getResponseBodyAsString());
            throw new NotFoundException(e.getStatusCode().value(), response.getError().getMessage());
        } catch (HttpClientErrorException e) {
            LOGGER.error("Error al obtener la poliza por vin", e);
            throw new AMISDispatchException("Error al conectar con el servicio de AMIS", HttpStatus.INTERNAL_SERVER_ERROR.value());

        }
    }

    /**
     * Obtiene el objeto poliza del Response obtenido (petici&oacute;n por VIN)
     * 
     * @param response Respesta obtenida de la petici&oacute;n
     * @return Objeto con los datos de Poliza
     */
    private List<DataResponseAmisDTO> getData(ResponseEntity<ResponseAmisDTO> response) {
        List<DataResponseAmisDTO> resultado = new ArrayList<>();
        if (response != null) {
            ResponseAmisDTO dto = response.getBody();
            if (dto != null) {
                resultado = dto.getData();
            }
        }
        return resultado;
    }

    /**
     * Obtiene el objeto poliza del Response obtenido (petici&oacute;n por poliza)
     * 
     * @param response Respesta obtenida de la petici&oacute;n
     * @return Objeto con los datos de Poliza
     */
    private PolizaDTO getPolizas(ResponseEntity<ResponseAmisPolizaDTO> response) {
        PolizaDTO result = null;
        if (response != null) {
            ResponseAmisPolizaDTO dto = response.getBody();
            if (dto != null) {
                result = dto.getData();
            }
        }
        return result;
    }

    private void initPrivateKey() {
        if (privateKey == null) {
            LOGGER.info("Desencriptando llave privada por primera vez");
            privateKey = decryptStringUtils.getRealValue(appProperties, appProperties.getProperty(FISEG_REST_AMIS_PRIVATE_KEY));
            LOGGER.debug("LLAVE OBTENIDA=> {}",privateKey);
            
        }
    }

}
