package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de una Cobertura valida para el FRONT (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "Objeto con la infomación del la cobertura")
public class CoberturaFinalDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -3996591414173464927L;

	@ApiModelProperty(notes = "Id de la cobertura")
	@JsonProperty("id")
	private String idCoverage;

	@ApiModelProperty(notes = "Estatus")
	@JsonProperty("status")
	private Integer status;

	@ApiModelProperty(notes = "Id de la suma asegurada")
	@JsonProperty("insuranceSumId")
	private Integer insuranceSumId;

	@ApiModelProperty(notes = "Suma asegurada")
	@JsonProperty("insuranceSum")
	private BigDecimal insuranceSum;

	/**
	 * @return the idCoverage
	 */
	public String getIdCoverage() {
		return idCoverage;
	}

	/**
	 * @param idCoverage the idCoverage to set
	 */
	public void setIdCoverage(String idCoverage) {
		this.idCoverage = idCoverage;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the insuranceSumId
	 */
	public Integer getInsuranceSumId() {
		return insuranceSumId;
	}

	/**
	 * @param insuranceSumId the insuranceSumId to set
	 */
	public void setInsuranceSumId(Integer insuranceSumId) {
		this.insuranceSumId = insuranceSumId;
	}

	/**
	 * @return the insuranceSum
	 */
	public BigDecimal getInsuranceSum() {
		return insuranceSum;
	}

	/**
	 * @param insuranceSum the insuranceSum to set
	 */
	public void setInsuranceSum(BigDecimal insuranceSum) {
		this.insuranceSum = insuranceSum;
	}

}