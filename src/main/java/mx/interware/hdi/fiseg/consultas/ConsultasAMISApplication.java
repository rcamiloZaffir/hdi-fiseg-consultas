package mx.interware.hdi.fiseg.consultas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * Clase principal para iniciar el microserivio consulta de serivicios Amis
 * 
 * @author Interware
 * @since 2020-10-16
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, SpringDataWebAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class })
@EnableSwagger2
@EnableOpenApi
public class ConsultasAMISApplication {
	private static final Logger LOGGER = LogManager.getLogger(ConsultasAMISApplication.class);

	@Value("${application.name}")
	private String applicationName;

	@Value("${application.version}")
	private String applicationVersion;

	/**
	 * Punto de acceso para iniciar principal para iniciar la aplicaci&oacute;n
	 *
	 * @param args argumentos de l&iacute;nea de comandos
	 */
	public static void main(String[] args) {
		SpringApplication.run(ConsultasAMISApplication.class, args);
	}

	/**
	 * Retorna un bean con la informaci&oacute;n del API
	 * 
	 * @return Bean con informaci&oacute;n del api
	 */
	private ApiInfo getApiInfo() {
		LOGGER.info("Retornando información de la aplicación");
		ApiInfo info = new ApiInfoBuilder().title("HDI " + applicationName + "\tAPI Info")
				.description("HDI " + applicationName + " API reference for developers").version(applicationVersion).build();
		LOGGER.info("{} ", info.getTitle());
		return info;
	}

	/**
	 * Retorna un bean para la generaci&oacute;n de los endpoints swagger2
	 * 
	 * @return Swagger configuration bean
	 */
	@Bean
	public Docket api() {
		LOGGER.info("Adding swagger support ...");
		Docket docket = new Docket(DocumentationType.OAS_30).groupName("public-api").apiInfo(getApiInfo()).select().paths(PathSelectors.any()).build();

		LOGGER.info("{} ", docket.getDocumentationType());
		return docket;
	}

	/**
	 * Retorna un bean con la configuraci&oacute;n del los hilos de ejecuci&oacute;n
	 * 
	 * @return Bean con configuracion&oacute;n de threads
	 */
	@Bean
	public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.setPoolSize(5);
		threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
		return threadPoolTaskScheduler;
	}
}
