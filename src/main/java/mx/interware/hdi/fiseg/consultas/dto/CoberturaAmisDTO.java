package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de una cobertura (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "Objeto con la infomación del la cobertura")
public class CoberturaAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -3996591414173464927L;

	@ApiModelProperty(notes = "Id de la cobertura")
	private String id;

	@ApiModelProperty(notes = "Estatus")
	private Integer estatus;

	@ApiModelProperty(notes = "Id de la suma asegurada")
	private Integer idSumaAsegurada;

	@ApiModelProperty(notes = "Suma asegurada")
	private BigDecimal sumaAsegurada;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the idSumaAsegurada
	 */
	public Integer getIdSumaAsegurada() {
		return idSumaAsegurada;
	}

	/**
	 * @param idSumaAsegurada the idSumaAsegurada to set
	 */
	public void setIdSumaAsegurada(Integer idSumaAsegurada) {
		this.idSumaAsegurada = idSumaAsegurada;
	}

	/**
	 * @return the sumaAsegurada
	 */
	public BigDecimal getSumaAsegurada() {
		return sumaAsegurada;
	}

	/**
	 * @param sumaAsegurada the sumaAsegurada to set
	 */
	public void setSumaAsegurada(BigDecimal sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

}