package mx.interware.hdi.fiseg.consultas.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;
import mx.interware.hdi.fiseg.consultas.service.ConsultasService;

/**
 * Controlador que funge como punto de acceso para las operaciones de consulta
 * de poliza AMIS
 * 
 * @author Interware
 * @since 2020-10-17
 */
@RestController
@RequestMapping("/api/v1/consultas")
@ApiModel(value = "ConsultasAMISController", description = "Punto de acceso para información de los elementos hacia AMIS")
public class ConsultasAMISController {
	private static final Logger LOGGER = LogManager.getLogger(ConsultasAMISController.class);

	@Autowired
	private ConsultasService consultasServiceImpl;

	/**
	 * Retorna una poliza de acuerdo al parametro enviado el cual puede ser:
	 * 'N&uacute;mero de poliza' o VIN
	 * 
	 * @param idElementoAMIS 'N&uacute;mero de poliza' o VIN
	 * @return Poliza encontrada
	 */
	@GetMapping("/{idElementoAMIS}")
	@ResponseBody
	@ApiOperation(value = "Obtiene los datos de la póliza", response = PolizaFinalDTO.class, notes = "Obtiene los datos a partir de una poliza o VIN, los cuales se encuentran en el servidor de AMIS")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 400, message = "Peticion Incorrecta"),
			@ApiResponse(code = 404, message = "No existe el recurso solicitado"),
			@ApiResponse(code = 500, message = "Error al procesar los datos") })

	public PolizaFinalDTO getPoliza(@PathVariable String idElementoAMIS) {
		LOGGER.info("GET /api/consultas/{}  estatus=START", idElementoAMIS);

		PolizaFinalDTO pDTO = consultasServiceImpl.getElementoAmisDTO(idElementoAMIS);
		
		LOGGER.debug("Se encontro poliza ={}", pDTO.getNumeroPolizaFinal());

		LOGGER.info("GET /api/consultas/{}  estatus=  END", idElementoAMIS);

		return pDTO;
	}

}
