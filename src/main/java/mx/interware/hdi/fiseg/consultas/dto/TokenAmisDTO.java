package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

/**
 * Respuesta de AMIS con el token de autenticaci&oacute;n
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class TokenAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = 4832991115631155878L;

	private String token;

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

}