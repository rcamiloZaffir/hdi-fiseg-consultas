package mx.interware.hdi.fiseg.consultas.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mx.interware.hdi.fiseg.consultas.dto.DetalleError;
import mx.interware.hdi.fiseg.consultas.exception.AMISDispatchException;
import mx.interware.hdi.fiseg.consultas.exception.NotFoundException;
import mx.interware.hdi.fiseg.consultas.exception.ValidationFieldServiceException;
import mx.interware.hdi.fiseg.consultas.util.DateUtils;

/**
 * Handler global para las excepciones en las peticiones
 *
 * @author Interware
 * @since 2020-10-17
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static final String CONSTANT_APP_ID = "X-Consultas-Amis";
	private static final String CONSTANT_ERROR = "error";

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	/**
	 * Encapsula el error porpagado del tipo {@link AMISDispatchException}
	 * 
	 * @param req Request de la operaci&oacute;n
	 * @param ex  Excepci&oacute;n propagada
	 * @return Objeto con el detalle de error
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(AMISDispatchException.class)
	public @ResponseBody ResponseEntity<DetalleError> handleBusinessException(HttpServletRequest req,
			AMISDispatchException ex) {
		LOGGER.info("handleBusinessException {} ", ex.getStatus());

		DetalleError detalle = new DetalleError();
		detalle.setMessage(ex.getMensaje());
		detalle.setStatus(ex.getStatus());
		detalle.setErrorTimestamp(DateUtils.getCurrentTimeStamp());
		detalle.setErrorType(DetalleError.ErrorType.HANDLED);

		Throwable causa = ex.getCause();

		if (causa != null) {
			LOGGER.error("ERROR: ", causa);

			if (causa instanceof AMISDispatchException) {
				LOGGER.error("ERROR: ", causa.getCause());
			} else {
				detalle.setErrorType(DetalleError.ErrorType.UNHANDLED);
			}
		}

		LOGGER.info("handleBusinessException status=   END ");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(detalle);
	}

	/**
	 * Encapsula el error porpapado del tipo ValidationFieldServiceException
	 * 
	 * @param req Request de la operaci&oacute;n
	 * @param ex  Excepci&oacute;n propagada
	 * @return Objeto con el detalle de error
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ValidationFieldServiceException.class)
	public @ResponseBody ResponseEntity<DetalleError> handleValidationFieldServiceException(HttpServletRequest req,
			ValidationFieldServiceException ex) {

		LOGGER.info("handleValidationFieldServiceException {} ", ex.getCode());

		DetalleError detalle = new DetalleError();
		detalle.setStatus(ex.getCode());
		detalle.setMessage(ex.getMessage());
		detalle.setErrorTimestamp(DateUtils.getCurrentTimeStamp());
		detalle.setErrorType(DetalleError.ErrorType.HANDLED);

		LOGGER.info("handleValidationFieldServiceException status=   END ");
		return new ResponseEntity<>(detalle, crearHeaderError(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}

	/**
	 * Encapsula el error porpapado del tipo NotFoundException
	 * 
	 * @param req Request de la operaci&oacute;n
	 * @param ex  Excepci&oacute;n propagada
	 * @return Objeto con el detalle de error
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler(NotFoundException.class)
	public @ResponseBody ResponseEntity<DetalleError> handleNotFoundException(HttpServletRequest req,
			NotFoundException ex) {

		LOGGER.info("handleNotFoundException {} ", ex.getStatus());

		DetalleError detalle = new DetalleError();
		detalle.setStatus(ex.getStatus());
		detalle.setMessage(ex.getMensaje());
		detalle.setErrorTimestamp(DateUtils.getCurrentTimeStamp());
		detalle.setErrorType(DetalleError.ErrorType.HANDLED);

		LOGGER.info("handleNotFoundException status=   END ");
		return new ResponseEntity<>(detalle, crearHeaderError(ex.getMessage()), HttpStatus.NO_CONTENT);
	}

	/**
	 * Metodo para formar un {@link HttpHeaders} de un proceso fallido
	 * 
	 * @param mensaje mensaje de error
	 * @return {@link HttpHeaders}
	 */
	public HttpHeaders crearHeaderError(String mensaje) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(CONSTANT_APP_ID + CONSTANT_ERROR, mensaje);
		return headers;
	}

}
