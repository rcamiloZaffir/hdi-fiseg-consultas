package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

/**
 * Respuesta de AMIS con el token de autenticaci&oacute;n
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class TokenRequestAmisDTO extends AmisDTO implements Serializable {
    private static final long serialVersionUID = 4832991115631155878L;

    private String certificate;
    private String privateKey;

    /**
     * @return the certificate
     */
    public String getCertificate() {
        return certificate;
    }

    /**
     * @param certificate the certificate to set
     */
    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    /**
     * @return the privateKey
     */
    public String getPrivateKey() {
        return privateKey;
    }

    /**
     * @param privateKey the privateKey to set
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((certificate == null) ? 0 : certificate.hashCode());
        result = prime * result + ((privateKey == null) ? 0 : privateKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TokenRequestAmisDTO other = (TokenRequestAmisDTO) obj;
        if (certificate == null) {
            if (other.certificate != null)
                return false;
        } else if (!certificate.equals(other.certificate))
            return false;
        if (privateKey == null) {
            if (other.privateKey != null)
                return false;
        } else if (!privateKey.equals(other.privateKey))
            return false;
        return true;
    }

}