package mx.interware.hdi.fiseg.consultas.exception;

/**
 * Clase que encapsula las excepciones de tipo 404
 * 
 * @author Interware
 * @since 2020-10-12
 */
public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Código de error.
	 */
	private final Integer status;
	private final String mensaje;

	/**
	 * Crea una nueva instancia de <code>NotFoundException</code>.
	 * <p>
	 * 
	 * @param status Estado de la excepci&oacute;n
	 * @param mensaje Descripci&oacute;n del error
	 */
	public NotFoundException(Integer status, String mensaje) {
		super(mensaje);
		this.status = status;
		this.mensaje = mensaje;
	}

	/**
	 * Retorn el mensaje de la excepci&oacute;n
	 * 
	 * @return Mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * Retorn el estado de la excepci&oacute;n
	 * 
	 * @return estatus
	 */
	public Integer getStatus() {
		return status;
	}

}
