package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Clase que contiene una respuesta de Amis (Consulta por VIN)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class ResponseAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -5759561147707718267L;

	private boolean success;
	private List<DataResponseAmisDTO> data;
	private ErrorAmisDTO error;

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the data
	 */
	public List<DataResponseAmisDTO> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<DataResponseAmisDTO> data) {
		this.data = data;
	}

	/**
	 * @return the error
	 */
	public ErrorAmisDTO getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(ErrorAmisDTO error) {
		this.error = error;
	}

}