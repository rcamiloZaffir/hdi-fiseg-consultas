package mx.interware.hdi.fiseg.consultas.util;

import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_INITVECTOR;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_ITERATIONS;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_KEYSIZE;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_PASSWORD;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_SALT;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import hdi.framework.security.cryptography.core.ICipher;
import hdi.framework.security.cryptography.symetric.AES256Cipher;

/**
 * Componente para obtener valores verdaderos de elementos encriptados
 * 
 * @author Interware
 * @since 2020-12-10
 *
 */
@Component
@Scope("prototype")
public class DecryptStringUtils {
    private static final Logger LOGGER = LogManager.getLogger(DecryptStringUtils.class);

    /**
     * Devuelve el valor real de una cade encriptada
     * 
     * @param app  Componente con las propiedades
     * @param text Texto a desencriptar
     * @return Valor Real
     */
    public String getRealValue(AppProperties app, String text) {

        String valorReal = "";
        String password = app.getProperty(CRYPT_PASSWORD);
        String salt = app.getProperty(CRYPT_SALT);
        int iterations = Integer.parseInt(app.getProperty(CRYPT_ITERATIONS));
        String initVector = app.getProperty(CRYPT_INITVECTOR);
        int keySize = Integer.parseInt(app.getProperty(CRYPT_KEYSIZE));

        ICipher cipher = new AES256Cipher(password, salt, iterations, initVector, keySize);

        try {
            valorReal = cipher.Decrypt(text);
            LOGGER.trace("Valor real=> {}", valorReal);
        } catch (Exception e) {
            LOGGER.error("Error al obtener el valor del elemento");
            valorReal = null;
        }
        return valorReal;
    }

}