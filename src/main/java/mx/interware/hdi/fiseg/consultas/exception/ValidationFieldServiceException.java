package mx.interware.hdi.fiseg.consultas.exception;

/**
 * Clase que encapsula las excepciones de tipo validaci&oacute;n de campos
 * 
 * @author Interware
 * @since 2020-10-17
 */
public class ValidationFieldServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final Integer code;

	/**
	 * Crea una instancia instance of <code>ValidationFieldServiceException</code>.
	 * <p>
	 * 
	 * @param code        C&oacute;digo de error
	 * @param campo       Campo que no cumple la valdacion
	 * @param descripcion Descripci&oacute;n del error
	 */
	public ValidationFieldServiceException(Integer code, String campo, String descripcion) {
		super("Error de validacion " + campo + " : " + descripcion);
		this.code = code;
	}

	/**
	 * Retorna el c&oacute;digo de la excepci&oacute;n
	 * 
	 * @return c&oacute;digo
	 */
	public Integer getCode() {
		return code;
	}

}
