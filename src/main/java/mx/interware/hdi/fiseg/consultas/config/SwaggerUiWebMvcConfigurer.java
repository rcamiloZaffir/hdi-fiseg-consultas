package mx.interware.hdi.fiseg.consultas.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 
 * Clase que permite la configuraci&oacute;n y redirecci&oacute;n de los
 * elementos de Swagger
 * 
 * @author Interware
 * @since 2020-10-17
 */
public class SwaggerUiWebMvcConfigurer implements WebMvcConfigurer {
	private static final Logger LOGGER = LogManager.getLogger(SwaggerUiWebMvcConfigurer.class);

	private final String baseUrl;

	/**
	 * Constructor a partir de la URL base
	 * 
	 * @param baseUrl URL base de la aplicaci&oacute;n
	 */
	public SwaggerUiWebMvcConfigurer(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String baseUrlFinal = StringUtils.trimTrailingCharacter(this.baseUrl, '/');

		StringBuilder resourceH = new StringBuilder(baseUrlFinal);
		resourceH.append("/swagger-ui/**");

		LOGGER.info("Creando configuracion => {}", resourceH);

		registry.addResourceHandler(resourceH.toString())
				.addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/")
				.resourceChain(false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController(baseUrl + "/swagger-ui/")
				.setViewName("forward:" + baseUrl + "/swagger-ui/index.html");
	}
}
