package mx.interware.hdi.fiseg.consultas.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO con las propiedades de un Beneficiario (Objeto AMIS)
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "Objeto con la infomación del Beneficiario")
public class BeneficiarioAmisDTO extends AmisDTO implements Serializable {
	private static final long serialVersionUID = -4138831860127141191L;

	@ApiModelProperty(notes = "Indicador del tipo de persona")
	private Short tipoPersona;

	@ApiModelProperty(notes = "Apellido paterno del beneficiario")
	private String apPaterno;
	@ApiModelProperty(notes = "Apellido materno del beneficiario")
	private String apMaterno;
	@ApiModelProperty(notes = "Nombre del beneficiario")
	private String nombre;

	@ApiModelProperty(notes = "RFC del beneficiario")
	@JsonProperty("RFC")
	private String rfc;

	@ApiModelProperty(notes = "CURP del beneficiario")
	@JsonProperty("CURP")
	private String curp;

	/**
	 * @return the tipoPersona
	 */
	public Short getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(Short tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * @return the apPaterno
	 */
	public String getApPaterno() {
		return apPaterno;
	}

	/**
	 * @param apPaterno the apPaterno to set
	 */
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	/**
	 * @return the apMaterno
	 */
	public String getApMaterno() {
		return apMaterno;
	}

	/**
	 * @param apMaterno the apMaterno to set
	 */
	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the curp
	 */
	public String getCurp() {
		return curp;
	}

	/**
	 * @param curp the curp to set
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}

}