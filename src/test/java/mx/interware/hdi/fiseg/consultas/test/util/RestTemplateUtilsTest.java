package mx.interware.hdi.fiseg.consultas.test.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;

import mx.interware.hdi.fiseg.consultas.util.RestTemplateUtils;

@RunWith(MockitoJUnitRunner.class)
public class RestTemplateUtilsTest {

  
    private static final Logger LOGGER = LogManager.getLogger(RestTemplateUtilsTest.class);

  
    @Test
    public void test_GetToken_OK() throws Exception {
        LOGGER.info("Probando disable");
        try {
            RestTemplateBuilder restTemplateBulid=new RestTemplateBuilder(null);
            RestTemplateUtils.disableSSL(restTemplateBulid);
        } catch (Exception e) {
            Assert.assertEquals(Boolean.TRUE, Boolean.TRUE);
        }
    }

}
