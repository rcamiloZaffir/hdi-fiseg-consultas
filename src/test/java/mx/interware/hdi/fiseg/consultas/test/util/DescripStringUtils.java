package mx.interware.hdi.fiseg.consultas.test.util;

import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_INITVECTOR;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_ITERATIONS;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_KEYSIZE;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_PASSWORD;
import static mx.interware.hdi.fiseg.consultas.util.Constants.CRYPT_SALT;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import mx.interware.hdi.fiseg.consultas.util.AppProperties;
import mx.interware.hdi.fiseg.consultas.util.DecryptStringUtils;

@RunWith(MockitoJUnitRunner.class)
public class DescripStringUtils {

    private static final Logger LOGGER = LogManager.getLogger(DescripStringUtils.class);

    private static final String CADENA_ORIGINAL = "CADENA_ORIGINAL_001";
    private static final String CADENA_ENCRYPT = "KKaw0eJp68JnxRI7GdFMfvFJqPz3eZik/CNVgj3ea5c=";

    @Mock
    private AppProperties appProperties;

    @InjectMocks
    private DecryptStringUtils decryptStringUtils;

    @Before
    public void setup() throws KeyManagementException, NoSuchAlgorithmException {
        MockitoAnnotations.initMocks(this);

        Mockito.when(appProperties.getProperty(CRYPT_PASSWORD)).thenReturn("1");
        Mockito.when(appProperties.getProperty(CRYPT_SALT)).thenReturn("12345678");
        Mockito.when(appProperties.getProperty(CRYPT_ITERATIONS)).thenReturn("1");
        Mockito.when(appProperties.getProperty(CRYPT_INITVECTOR)).thenReturn("1234567890123451");
        Mockito.when(appProperties.getProperty(CRYPT_KEYSIZE)).thenReturn("256");

    }

    @Test
    public void test_GetToken_OK() throws Exception {
        LOGGER.info("Probando el retorno de una token correcto");

        String realValue = decryptStringUtils.getRealValue(appProperties, CADENA_ENCRYPT);
        LOGGER.info("Real Value=> {}", realValue);
        Assert.assertEquals(CADENA_ORIGINAL, realValue);
    }

}
