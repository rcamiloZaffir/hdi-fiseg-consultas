package mx.interware.hdi.fiseg.consultas.test.rest;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@RunWith(SpringRunner.class)
@SpringBootTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
public class HealthControllerTest {
	private static final Logger LOGGER = LogManager.getLogger(HealthControllerTest.class);

	@Autowired
	protected MockMvc mvc;

	@Test
	public void pingTest() throws Exception {
		LOGGER.info("Testing ping method...");
		String expected = "pong";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/ping");
		ResultActions ra = mvc.perform(requestBuilder);
		LOGGER.info(" {}", ra.andExpect(status().isOk()));
		ra.andExpect(status().isOk()).andExpect(content().string(equalTo(expected)));
	}

	@Test
	public void healthTest() throws Exception {
		LOGGER.info("Testing health method ...");
		String expected = "{'action':'verifyHealth', 'key':'kptzin', 'status':'OK'}";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/health/kptzin");
		mvc.perform(requestBuilder).andExpect(status().isOk()).andExpect(content().string(equalTo(expected)));
	}

	@Test
	public void helloTest() throws Exception {
		LOGGER.info("Testing sayHi method ...");
		String name = "kptzin";
		String expected = "Hello there " + name + "!";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/sayHi/" + name);
		mvc.perform(requestBuilder).andExpect(status().isOk()).andExpect(content().string(equalTo(expected)));
	}

}
