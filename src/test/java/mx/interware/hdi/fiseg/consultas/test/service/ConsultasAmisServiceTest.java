package mx.interware.hdi.fiseg.consultas.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import mx.interware.hdi.fiseg.consultas.dto.CoberturaAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;
import mx.interware.hdi.fiseg.consultas.dto.TipoBusqueda;
import mx.interware.hdi.fiseg.consultas.exception.ValidationFieldServiceException;
import mx.interware.hdi.fiseg.consultas.rest.RestCaller;
import mx.interware.hdi.fiseg.consultas.service.ConsultasServiceImpl;
import mx.interware.hdi.fiseg.consultas.util.ConvertPolizaUtils;

@RunWith(MockitoJUnitRunner.class)
public class ConsultasAmisServiceTest {

    private static final Logger LOGGER = LogManager.getLogger(ConsultasAmisServiceTest.class);

    @Mock
    private RestCaller restCaller;

    @InjectMocks
    private ConsultasServiceImpl consultasServiceImpl;

    @Before
    public void setup() {
        PolizaDTO polizaFull = new PolizaDTO();
        PolizaDTO polizaNotFull = new PolizaDTO();
        CoberturaAmisDTO c = new CoberturaAmisDTO();

        List<CoberturaAmisDTO> coberturas = new ArrayList<>();
        coberturas.add(c);
        polizaFull.setCoberturas(coberturas);
        polizaFull.setFechaInicioVigencia("01/01/2020");
        polizaFull.setFechaFinalVigencia("01/01/2021");
        polizaFull.setFechaEmision("01-01-2020");
        MockitoAnnotations.initMocks(this);
        Mockito.when(restCaller.getToken()).thenReturn("");
        Mockito.when(restCaller.getByPoliza("13-666668-1", "")).thenReturn(polizaFull);
        Mockito.when(restCaller.getByVin("1GKER13707J140666", "")).thenReturn(polizaNotFull);
    }

    @Test
    public void test_400_BadRequest() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta");
        String polizaCorrecta = "ABCDEF-101010101010101-1010101";
        try {
            consultasServiceImpl.getElementoAmisDTO(polizaCorrecta);
        } catch (ValidationFieldServiceException e) {
            assertTrue(Boolean.TRUE);
        }
    }

    @Test
    public void test_GetByPoliza_OK() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta");
        String polizaCorrecta = "13-666668-1";
        PolizaFinalDTO pDTO = consultasServiceImpl.getElementoAmisDTO(polizaCorrecta);
        LOGGER.info("Poliza => {}", pDTO);
        assertNotNull(pDTO);
        assertEquals(TipoBusqueda.POLIZA.toString(), pDTO.getTipoBusquedaFinal());
        assertEquals(ConvertPolizaUtils.MESSAGE_DATE, pDTO.getFechaEmisionFinal());
        assertEquals("20210101", pDTO.getFechaFinalVigenciaFinal());
        assertEquals("20200101", pDTO.getFechaInicioVigenciaFinal());
    }

    @Test
    public void test_GetByVin_OK() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta");
        String polizaCorrecta = "1GKER13707J140666";
        PolizaFinalDTO pDTO = consultasServiceImpl.getElementoAmisDTO(polizaCorrecta);
        LOGGER.info("Poliza => {}", pDTO);
        assertNotNull(pDTO);
        assertEquals(TipoBusqueda.VIN.toString(), pDTO.getTipoBusquedaFinal());
    }
}
