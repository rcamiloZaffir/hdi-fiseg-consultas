package mx.interware.hdi.fiseg.consultas.test.rest;

import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;
import mx.interware.hdi.fiseg.consultas.rest.ConsultasAMISController;
import mx.interware.hdi.fiseg.consultas.service.ConsultasService;

@RunWith(MockitoJUnitRunner.class)
public class ConsultasAMISControllerTest {

	private static final Logger LOGGER = LogManager.getLogger(ConsultasAMISControllerTest.class);

	@Mock
	private ConsultasService consultasService;

	@InjectMocks
	private ConsultasAMISController consultasAMISController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(consultasService.getElementoAmisDTO("13-666668-1")).thenReturn(new PolizaFinalDTO());
	}

	@Test
	public void test_GetByPoliza_OK() throws Exception {
		LOGGER.info("Probando el retorno de una poliza correcta");
		String polizaCorrecta = "13-666668-1";
		PolizaFinalDTO pDTO = consultasAMISController.getPoliza(polizaCorrecta);
		LOGGER.info("Poliza => {}", pDTO);
		assertNotNull(pDTO);
	}

}
