package mx.interware.hdi.fiseg.consultas.test.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import mx.interware.hdi.fiseg.consultas.dto.DataResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisPolizaDTO;
import mx.interware.hdi.fiseg.consultas.exception.AMISDispatchException;
import mx.interware.hdi.fiseg.consultas.util.JsonUtils;

public class JsonUtilTest {

    private static final Logger LOGGER = LogManager.getLogger(JsonUtilTest.class);

    @Test
    public void testAppProperties_GetDate() {
        LOGGER.info("Iniciando test para parseo de Json");
        PolizaDTO dto = new PolizaDTO();
        dto.setAgente("AGENTE");
        dto.setCanalVenta(1);

        String json = JsonUtils.toAmisJson(dto);

        LOGGER.info("Objeto parseado=> {}", json);
        Assert.assertTrue(json.contains("agente"));
        Assert.assertTrue(json.contains("AGENTE"));
        Assert.assertTrue(json.contains("canalVenta"));
        Assert.assertTrue(json.contains("1"));
    }

    @Test
    public void testAppProperties_DeserialiseResponseAmisPolizaDTO() {
        LOGGER.info("Iniciando test para parseo de PolizaDTO");
        PolizaDTO pol = new PolizaDTO();
        pol.setAgente("AGENTE1");

        List<PolizaDTO> lista = new ArrayList<>();
        lista.add(pol);

        ResponseAmisPolizaDTO dto = new ResponseAmisPolizaDTO();
        dto.setSuccess(true);
        dto.setData(lista.get(0));

        String json = JsonUtils.toAmisJson(dto);

        ResponseAmisPolizaDTO result = JsonUtils.deserializeResponseAmisPoliza(json);

        Assert.assertEquals(result.getData().getAgente(), pol.getAgente());
    }

    @Test
    public void testAppProperties_DeserialiseResponseAmisDTO() {
        LOGGER.info("Iniciando test para parseo de PolizaDTO");
        PolizaDTO pol = new PolizaDTO();
        pol.setAgente("AGENTE1");

        DataResponseAmisDTO dataRes = new DataResponseAmisDTO();
        dataRes.setRecord(pol);

        List<DataResponseAmisDTO> lista = new ArrayList<>();
        lista.add(dataRes);

        ResponseAmisDTO dto = new ResponseAmisDTO();
        dto.setSuccess(true);
        dto.setData(lista);

        String json = JsonUtils.toAmisJson(dto);

        ResponseAmisDTO result = JsonUtils.deserializeResponseAmis(json);

        Assert.assertEquals(result.getData().get(0).getRecord().getAgente(), pol.getAgente());
    }

    @Test
    public void test_deserializeToken_ERROR() {
        try {
            JsonUtils.deserializeToken("A");
            Assert.assertTrue(Boolean.FALSE);
        } catch (AMISDispatchException e) {
            Assert.assertNotNull(e);
            Assert.assertTrue(Boolean.TRUE);
        }
    }
    
    @Test
    public void test_deserializeResponseAmisPoliza_ERROR() {
        try {
            JsonUtils.deserializeResponseAmisPoliza("A");
            Assert.assertTrue(Boolean.FALSE);
        } catch (AMISDispatchException e) {
            Assert.assertNotNull(e);
            Assert.assertTrue(Boolean.TRUE);
        }
    }
    

    @Test
    public void test_deserializeResponseAmis_ERROR() {
        try {
            JsonUtils.deserializeResponseAmis("A");
            Assert.assertTrue(Boolean.FALSE);
        } catch (AMISDispatchException e) {
            Assert.assertNotNull(e);
            Assert.assertTrue(Boolean.TRUE);
        }
    }

}
