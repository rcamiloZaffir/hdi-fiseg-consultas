package mx.interware.hdi.fiseg.consultas.test.handler;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import mx.interware.hdi.fiseg.consultas.dto.DetalleError;
import mx.interware.hdi.fiseg.consultas.exception.AMISDispatchException;
import mx.interware.hdi.fiseg.consultas.exception.NotFoundException;
import mx.interware.hdi.fiseg.consultas.exception.ValidationFieldServiceException;
import mx.interware.hdi.fiseg.consultas.handler.GlobalExceptionHandler;

public class GlobalExceptionHandlerTest {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandlerTest.class);

    private GlobalExceptionHandler globalExceptionHandler;

    @Before
    public void setup() {
        globalExceptionHandler = new GlobalExceptionHandler();
    }

    @Test
    public void executeHandlerAmisDispatch_TEST() {
        LOG.info("executeHandlerAmisDispatch_TEST   status = START");
        AMISDispatchException  error = new AMISDispatchException("Mensaje Test",HttpStatus.INTERNAL_SERVER_ERROR.value());
        ResponseEntity<DetalleError> de = globalExceptionHandler.handleBusinessException(null, error);
        assertNotNull(de);
        assertEquals("Mensaje Test", de.getBody().getMessage());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), de.getBody().getStatus());
    }
    

    @Test
    public void executeHandlerAmisDispatch_TEST01() {
        LOG.info("executeHandlerAmisDispatch_TEST   status = START");
        AMISDispatchException  error = new AMISDispatchException("MensajeNew",new Exception());
        ResponseEntity<DetalleError> de = globalExceptionHandler.handleBusinessException(null, error);
        assertNotNull(de);
        assertEquals("MensajeNew", de.getBody().getMessage());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), de.getBody().getStatus());
    }
    
    @Test
    public void executeHandlerAmisDispatch_TEST0() {
        LOG.info("executeHandlerAmisDispatch_TEST   status = START");
        AMISDispatchException  error = new AMISDispatchException(new Exception("MensajeNew"));
        ResponseEntity<DetalleError> de = globalExceptionHandler.handleBusinessException(null, error);
        assertNotNull(de);
        assertEquals("Error al procesar la informacion", de.getBody().getMessage());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), de.getBody().getStatus());
    }
    
    @Test
    public void executeHandlerValidationField_TEST() {
    	
        LOG.info("executeHandlerValidationField_TEST   status = START");
        ValidationFieldServiceException vfse=new ValidationFieldServiceException(HttpStatus.BAD_REQUEST.value(), "ID", "No Valido");
        ResponseEntity<DetalleError> de = globalExceptionHandler.handleValidationFieldServiceException(null, vfse);
        assertNotNull(de);
        assertEquals(HttpStatus.BAD_REQUEST.value(), de.getBody().getStatus());
    }
    

    @Test
    public void executeHandleNotFoundExceptionTEST() {
        LOG.info("executeHandlerValidationField_TEST   status = START");
        NotFoundException nfe=new NotFoundException(HttpStatus.NOT_FOUND.value(), "No encontrado");
        ResponseEntity<DetalleError> de = globalExceptionHandler.handleNotFoundException(null, nfe);
        assertNotNull(de);
        assertEquals(HttpStatus.NOT_FOUND.value(), de.getBody().getStatus());
        assertEquals("No encontrado", de.getBody().getMessage());
    }


}
