package mx.interware.hdi.fiseg.consultas.test.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import mx.interware.hdi.fiseg.consultas.dto.BeneficiarioAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ClienteAMISDTO;
import mx.interware.hdi.fiseg.consultas.dto.CoberturaAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.CoberturaFinalDTO;
import mx.interware.hdi.fiseg.consultas.dto.DetalleError;
import mx.interware.hdi.fiseg.consultas.dto.DetalleError.ErrorType;
import mx.interware.hdi.fiseg.consultas.dto.ErrorAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaFinalDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisPolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.VehiculoAmisDTO;

public class DTOTest {

	private static final String RFC = "RFC";
	private static final String CURP = "CURP";
	private static final String NOMBRE = "NOMBRE";
	private static final String AP_PAT = "AP_PAT";
	private static final String AP_MAT = "AP_MAT";
	private static final String ID_STRING = "ID_STRING";
	private static final Integer STATUS = 1;
	private static final Integer ID_SUMA_ASEG = 1;
	private static final BigDecimal SUMA_ASEGUR = new BigDecimal(100.0);
	private static final short TIPO_PERSONA = 1;

	private static final Integer NUMBER_INT = 1;
	private static final Short NUMBER_SHORT = 1;
	private static final String STRING = "Str";

	@Test
	public void test_ClienteAMISDTO() {

		ClienteAMISDTO dto = new ClienteAMISDTO();
		dto.setApPaternoCliente(AP_PAT);
		dto.setApMaternoCliente(AP_MAT);
		dto.setNombreCliente(NOMBRE);
		dto.setRfcCliente(RFC);
		dto.setCurpCliente(CURP);

		Assert.assertEquals(dto.getApPaternoCliente(), AP_PAT);
		Assert.assertEquals(dto.getApMaternoCliente(), AP_MAT);
		Assert.assertEquals(dto.getNombreCliente(), NOMBRE);
		Assert.assertEquals(dto.getRfcCliente(), RFC);
		Assert.assertEquals(dto.getCurpCliente(), CURP);
	}

	@Test
	public void test_BeneficiarioAMISDTO() {

		BeneficiarioAmisDTO dto = new BeneficiarioAmisDTO();
		dto.setApPaterno(AP_PAT);
		dto.setApMaterno(AP_MAT);
		dto.setNombre(NOMBRE);
		dto.setRfc(RFC);
		dto.setCurp(CURP);
		dto.setTipoPersona(TIPO_PERSONA);
		
		Assert.assertEquals(dto.getApPaterno(), AP_PAT);
		Assert.assertEquals(dto.getApMaterno(), AP_MAT);
		Assert.assertEquals(dto.getNombre(), NOMBRE);
		Assert.assertEquals(dto.getRfc(), RFC);
		Assert.assertEquals(dto.getCurp(), CURP);
		Assert.assertNotNull(dto.getTipoPersona());
	}

	@Test
	public void test_CoberturaAMISDTO() {

		CoberturaAmisDTO dto = new CoberturaAmisDTO();
		dto.setId(ID_STRING);
		dto.setEstatus(STATUS);
		dto.setIdSumaAsegurada(ID_SUMA_ASEG);
		dto.setSumaAsegurada(SUMA_ASEGUR);

		Assert.assertEquals(dto.getId(), ID_STRING);
		Assert.assertEquals(dto.getEstatus(), STATUS);
		Assert.assertEquals(dto.getIdSumaAsegurada(), ID_SUMA_ASEG);
		Assert.assertEquals(dto.getSumaAsegurada(), SUMA_ASEGUR);
	}

	@Test
	public void test_CoberturaFinalDTO() {

		CoberturaFinalDTO dto = new CoberturaFinalDTO();
		dto.setIdCoverage(ID_STRING);
		dto.setStatus(STATUS);
		dto.setInsuranceSumId(ID_SUMA_ASEG);
		dto.setInsuranceSum(SUMA_ASEGUR);

		Assert.assertEquals(dto.getIdCoverage(), ID_STRING);
		Assert.assertEquals(dto.getStatus(), STATUS);
		Assert.assertEquals(dto.getInsuranceSumId(), ID_SUMA_ASEG);
		Assert.assertEquals(dto.getInsuranceSum(), SUMA_ASEGUR);
	}

	@Test
	public void test_VehiculoAmisDTO() {

		List<CoberturaAmisDTO> coberturas = new ArrayList<>();

		VehiculoAmisDTO dto = new VehiculoAmisDTO();
		dto.setUso(NUMBER_INT);
		dto.setVin(STRING);
		dto.setInciso(STRING);
		dto.setIncisoEstatus(STATUS);
		dto.setMarcaTipo(NUMBER_INT);
		dto.setModelo(NUMBER_INT);
		dto.setTipoMovimiento(NUMBER_INT);
		dto.setPlaca(STRING);
		dto.setTipoSeguro(NUMBER_SHORT);
		dto.setCoberturas(coberturas);

		Assert.assertEquals(dto.getUso(), NUMBER_INT);
		Assert.assertEquals(dto.getVin(), STRING);
		Assert.assertEquals(dto.getInciso(), STRING);
		Assert.assertEquals(dto.getIncisoEstatus(), STATUS);
		Assert.assertEquals(dto.getMarcaTipo(), NUMBER_INT);
		Assert.assertEquals(dto.getModelo(), NUMBER_INT);
		Assert.assertEquals(dto.getTipoMovimiento(), NUMBER_INT);
		Assert.assertEquals(dto.getPlaca(), STRING);
		Assert.assertEquals(dto.getTipoSeguro(), NUMBER_SHORT);
		Assert.assertEquals(dto.getCoberturas().size(), 0);
	}

	private static final String AGENTE = "AGENTE";

	@Test
	public void test_PolizaDTO() {
		List<BeneficiarioAmisDTO> beneficiarios = new ArrayList<>();
		List<ClienteAMISDTO> clientes = new ArrayList<>();
		List<VehiculoAmisDTO> vehiculos = new ArrayList<>();
		List<CoberturaAmisDTO> coberturas = new ArrayList<>();

		PolizaDTO dto = new PolizaDTO();
		dto.setAgente(AGENTE);
		dto.setCanalVenta(NUMBER_INT);
		dto.setDoctype(STRING);
		dto.setNumeroPoliza(ID_STRING);
		dto.setNumeroReferencia(STRING + ID_STRING);
		dto.setBeneficiarios(beneficiarios);
		dto.setClientes(clientes);
		dto.setCoberturas(coberturas);
		dto.setVehiculos(vehiculos);

		Assert.assertEquals(dto.getAgente(), AGENTE);
		Assert.assertEquals(dto.getCanalVenta(), NUMBER_INT);
		Assert.assertEquals(dto.getDoctype(), STRING);
		Assert.assertEquals(dto.getNumeroPoliza(), ID_STRING);
		Assert.assertEquals(dto.getNumeroReferencia(), STRING + ID_STRING);
		Assert.assertEquals(dto.getBeneficiarios().size(), 0);
		Assert.assertEquals(dto.getClientes().size(), 0);
		Assert.assertEquals(dto.getCoberturas().size(), 0);
		Assert.assertEquals(dto.getVehiculos().size(), 0);

	}

	@Test
	public void test_PolizaFinalDTO() {
		List<BeneficiarioAmisDTO> beneficiarios = new ArrayList<>();
		List<ClienteAMISDTO> clientes = new ArrayList<>();
		List<VehiculoAmisDTO> vehiculos = new ArrayList<>();
		List<CoberturaFinalDTO> coberturas = new ArrayList<>();

		PolizaFinalDTO dto = new PolizaFinalDTO();
		dto.setAgenteFinal(AGENTE);
		dto.setCanalVentaFinal(NUMBER_INT);
		dto.setDoctypeFinal(STRING);
		dto.setNumeroPolizaFinal(ID_STRING);
		dto.setNumeroReferenciaFinal(STRING + ID_STRING);
		dto.setBeneficiarios(beneficiarios);
		dto.setClientes(clientes);
		dto.setCoverage(coberturas);
		dto.setVehicles(vehiculos);

		Assert.assertEquals(dto.getAgenteFinal(), AGENTE);
		Assert.assertEquals(dto.getCanalVentaFinal(), NUMBER_INT);
		Assert.assertEquals(dto.getDoctypeFinal(), STRING);
		Assert.assertEquals(dto.getNumeroPolizaFinal(), ID_STRING);
		Assert.assertEquals(dto.getNumeroReferenciaFinal(), STRING + ID_STRING);
		Assert.assertEquals(dto.getBeneficiarios().size(), 0);
		Assert.assertEquals(dto.getClientes().size(), 0);
		Assert.assertEquals(dto.getCoverage().size(), 0);
		Assert.assertEquals(dto.getVehicles().size(), 0);
	}

	@Test
	public void test_ResponseAmisPolizaDTO() {
		ErrorAmisDTO error = new ErrorAmisDTO();
		error.setMessage(STRING);
		error.setStatus(NUMBER_INT);
		ResponseAmisPolizaDTO dto = new ResponseAmisPolizaDTO();
		dto.setError(error);
		dto.setSuccess(true);

		Assert.assertEquals(dto.isSuccess(), true);
		Assert.assertEquals(dto.getError().getMessage(), STRING);
		Assert.assertEquals(dto.getError().getStatus(), NUMBER_INT);
	}

	@Test
	public void test_ResponsePolizaDTO() {
		ErrorAmisDTO error = new ErrorAmisDTO();
		ResponseAmisDTO dto = new ResponseAmisDTO();
		dto.setError(error);
		dto.setSuccess(true);

		Assert.assertEquals(dto.isSuccess(), true);
		Assert.assertNotNull(dto.getError() );
	}



	@Test
	public void test_DetalleError() {
		DetalleError dto = new DetalleError();
		dto.setErrorType(ErrorType.HANDLED);
		dto.setErrorTimestamp(STRING);

		Assert.assertEquals(dto.getErrorTimestamp(), STRING);
		Assert.assertEquals(dto.getErrorType(),ErrorType.HANDLED );
	}}
