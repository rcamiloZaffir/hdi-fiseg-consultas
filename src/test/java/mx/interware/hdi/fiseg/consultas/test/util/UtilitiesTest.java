package mx.interware.hdi.fiseg.consultas.test.util;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import mx.interware.hdi.fiseg.consultas.util.AppProperties;
import mx.interware.hdi.fiseg.consultas.util.DateUtils;

public class UtilitiesTest {

    private static Logger log = LogManager.getLogger(UtilitiesTest.class);

    @Test
    public void testDateUtils_parseYmdhms() {
        log.info("Iniciando test para parseo de fechas");

        String fecha = "2020-10-09 23:39:00.000";// yyyy-MM-dd hh:mm:ss.SSS
        log.info("String entrada => {}", fecha);

        Calendar c = Calendar.getInstance();
        c.set(2020, Calendar.OCTOBER, 9, 23, 39, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date expected = new Date(c.getTimeInMillis());
        log.info("Fecha Esperada => {}", expected.toString());
        try {
            Date result = DateUtils.parseYmdhms(fecha);
            log.info("Resultado => {}", result.toString());
            Assert.assertEquals(expected, result);
        } catch (ParseException e) {
            log.error("Error al realizar el parseo de fecha, Resultado no esperado");
            Assert.assertTrue(Boolean.FALSE);
        }

    }

    @Test
    public void testAppProperties_GetDate() {
        log.info("Iniciando test para parseo de fechas");

        Assert.assertNotNull(DateUtils.getCurrentTimeStamp());
    }

    @Test
    public void testAppProperties_GetPropertie() {
        log.info("Iniciando test para parseo de fechas");

        AppProperties app = new AppProperties();
        String notExist = app.getProperty("NOT_EXIST");
        Assert.assertNull(notExist);
    }
}
