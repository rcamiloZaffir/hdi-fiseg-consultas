package mx.interware.hdi.fiseg.consultas.test.rest;

import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AMIS_CERTIFICATE;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AMIS_PRIVATE_KEY;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_AUTHENTICATION_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_POLIZA_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_REST_VIN_ENDPOINT;
import static mx.interware.hdi.fiseg.consultas.util.Constants.FISEG_SSL_VALIDATION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import mx.interware.hdi.fiseg.consultas.dto.DataResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ErrorAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.PolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisDTO;
import mx.interware.hdi.fiseg.consultas.dto.ResponseAmisPolizaDTO;
import mx.interware.hdi.fiseg.consultas.dto.TokenRequestAmisDTO;
import mx.interware.hdi.fiseg.consultas.exception.NotFoundException;
import mx.interware.hdi.fiseg.consultas.rest.RestCaller;
import mx.interware.hdi.fiseg.consultas.util.AppProperties;
import mx.interware.hdi.fiseg.consultas.util.DecryptStringUtils;

@RunWith(MockitoJUnitRunner.class)
public class RestControllerTest {

    private static final Logger LOGGER = LogManager.getLogger(RestControllerTest.class);

    @Mock
    private AppProperties appProperties;

    @Mock
    private DecryptStringUtils decryptStringUtils;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private RestTemplateBuilder restTemplateBulid;

    private RestCaller restCaller;

    private static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";

    private static final String URL_AUT = "https://apicaamis.mybluemix.net/v2/authentication/";
    private static final String URL_POL = "https://apicaamis.mybluemix.net/v2/poliza/";
    private static final String URL_VIN = "https://apicaamis.mybluemix.net/v2/poliza/vin/";
    private static final String POLIZA_VALIDA = "13-666668-1";
    private static final String POLIZA_NOT_FOUND = "13-666668-2";
    private static final String VIN_VALIDO = "1GKER13707J140666";
    private static final String VIN_SIN_DATA = "1GKER13707J140667";
    private static final String VIN_NOT_FOUND = "1GKER13707J140668";
    private static final String AGENTE = "Agente Test";

    @Before
    public void setup() throws KeyManagementException, NoSuchAlgorithmException {
        MockitoAnnotations.initMocks(this);

        PolizaDTO poliza = new PolizaDTO();
        poliza.setAgente(AGENTE);

        List<PolizaDTO> polizas = new ArrayList<>();
        polizas.add(poliza);
        ResponseAmisPolizaDTO responseAmisPoliza = new ResponseAmisPolizaDTO();
        responseAmisPoliza.setSuccess(true);
        responseAmisPoliza.setData(polizas.get(0));
        ResponseAmisPolizaDTO responseAmisPolizaNotData = new ResponseAmisPolizaDTO();
        responseAmisPolizaNotData.setSuccess(true);

        DataResponseAmisDTO data = new DataResponseAmisDTO();
        data.setKey("AA");
        data.setRecord(poliza);
        List<DataResponseAmisDTO> dataList = new ArrayList<DataResponseAmisDTO>();
        dataList.add(data);

        ResponseAmisDTO responseVin = new ResponseAmisDTO();
        responseVin.setSuccess(true);
        responseVin.setData(dataList);

        ErrorAmisDTO errorAmis = new ErrorAmisDTO();
        errorAmis.setMessage("-");
        errorAmis.setStatus(400);
        ResponseAmisDTO responseVinError = new ResponseAmisDTO();
        responseVinError.setError(errorAmis);

        ResponseAmisDTO responseVinNotFound = new ResponseAmisDTO();
        responseVinNotFound.setSuccess(true);
        responseVinNotFound.setData(new ArrayList<DataResponseAmisDTO>());

        ResponseEntity<String> responseTokenOK = new ResponseEntity<>("{ \"token\": \"" + TOKEN + "\"}", HttpStatus.OK);
        ResponseEntity<ResponseAmisPolizaDTO> responsePolizaOK = new ResponseEntity<>(responseAmisPoliza, HttpStatus.OK);

        ResponseEntity<ResponseAmisPolizaDTO> responsePolizaNotData = new ResponseEntity<>(responseAmisPolizaNotData, HttpStatus.OK);
        ResponseEntity<ResponseAmisDTO> responseVinOK = new ResponseEntity<>(responseVin, HttpStatus.OK);
        ResponseEntity<ResponseAmisDTO> responseVinNotData = new ResponseEntity<>(responseVinNotFound, HttpStatus.OK);
        ResponseEntity<ResponseAmisDTO> responseVinHashError = new ResponseEntity<>(responseVinError, HttpStatus.BAD_REQUEST);

        TokenRequestAmisDTO tokenRequest = new TokenRequestAmisDTO();
        tokenRequest.setCertificate("--AB--");
        tokenRequest.setPrivateKey("pvk");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<TokenRequestAmisDTO> request = new HttpEntity<>(tokenRequest, headers);

        HttpHeaders headersWithToken = new HttpHeaders();
        headersWithToken.set("access-token", TOKEN);
        HttpEntity<String> requestWithToken = new HttpEntity<>(headersWithToken);

        Mockito.when(appProperties.getProperty(FISEG_REST_AMIS_PRIVATE_KEY)).thenReturn("pvk");
        Mockito.when(appProperties.getProperty(FISEG_REST_AMIS_CERTIFICATE)).thenReturn("--AB--");
        Mockito.when(appProperties.getProperty(FISEG_SSL_VALIDATION)).thenReturn("true");
        Mockito.when(appProperties.getProperty(FISEG_REST_AUTHENTICATION_ENDPOINT)).thenReturn(URL_AUT);
        Mockito.when(appProperties.getProperty(FISEG_REST_POLIZA_ENDPOINT)).thenReturn(URL_POL);
        Mockito.when(appProperties.getProperty(FISEG_REST_VIN_ENDPOINT)).thenReturn(URL_VIN);

        Mockito.when(restTemplate.postForEntity(URL_AUT, request, String.class)).thenReturn(responseTokenOK);
        Mockito.when(restTemplate.exchange(URL_POL + POLIZA_VALIDA, HttpMethod.GET, requestWithToken, ResponseAmisPolizaDTO.class))
                .thenReturn(responsePolizaOK);
        Mockito.when(restTemplate.exchange(URL_POL + POLIZA_NOT_FOUND, HttpMethod.GET, requestWithToken, ResponseAmisPolizaDTO.class))
                .thenReturn(responsePolizaNotData);

        Mockito.when(decryptStringUtils.getRealValue(appProperties, "pvk")).thenReturn("pvk");

        Mockito.when(restTemplate.exchange(URL_VIN + VIN_VALIDO, HttpMethod.GET, requestWithToken, ResponseAmisDTO.class)).thenReturn(responseVinOK);
        Mockito.when(restTemplate.exchange(URL_VIN + VIN_SIN_DATA, HttpMethod.GET, requestWithToken, ResponseAmisDTO.class)).thenReturn(responseVinNotData);
        Mockito.when(restTemplate.exchange(URL_VIN + VIN_NOT_FOUND, HttpMethod.GET, requestWithToken, ResponseAmisDTO.class)).thenReturn(responseVinHashError);

        Mockito.when(restTemplateBulid.build()).thenReturn(restTemplate);
        restCaller = new RestCaller(restTemplateBulid, appProperties);

        try {
            FieldUtils.writeField(restCaller, "decryptStringUtils", decryptStringUtils, true);
        } catch (Exception e) {
            LOGGER.info("Error al setear los elementos {}", e);
        }

    }

    @Test
    public void test_GetToken_OK() throws Exception {
        LOGGER.info("Probando el retorno de una token correcto");

        String token = restCaller.getToken();
        LOGGER.info("Token => {}", token);
        assertNotNull(token);
        assertEquals(TOKEN, token);
    }

    @Test
    public void test_GetByPoliza_OK() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta a traves de Poliza");
        LOGGER.info("URL => {}{}", URL_POL, POLIZA_VALIDA);
        PolizaDTO poliza = restCaller.getByPoliza(POLIZA_VALIDA, TOKEN);
        assertNotNull(poliza);
        assertEquals(AGENTE, poliza.getAgente());
    }

    @Test
    public void test_GetByPoliza_NOT_FOUND() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta a traves de Poliza");
        LOGGER.info("URL => {}{}", URL_POL, POLIZA_NOT_FOUND);
        try {
            restCaller.getByPoliza(POLIZA_NOT_FOUND, TOKEN);
        } catch (NotFoundException e) {
            assertEquals(e.getMensaje(), "No se encontró elemento");
        }
    }

    @Test
    public void test_GetByVin_OK() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta a traves de VIN");

        LOGGER.info("URL => {}{}", URL_VIN, VIN_VALIDO);
        PolizaDTO poliza = restCaller.getByVin(VIN_VALIDO, TOKEN);
        assertNotNull(poliza);
        assertEquals(AGENTE, poliza.getAgente());

    }

    @Test
    public void test_GetByVin_NOT_DATA() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta a traves de Vin");
        LOGGER.info("URL => {}{}", URL_VIN, VIN_SIN_DATA);
        try {
            restCaller.getByVin(VIN_SIN_DATA, TOKEN);
        } catch (NotFoundException e) {
            assertEquals(e.getMensaje(), "No se encontró elemento");
        }
    }

    @Test
    public void test_GetByVin_NOT_FOUND() throws Exception {
        LOGGER.info("Probando el retorno de una poliza correcta a traves de Vin");
        LOGGER.info("URL => {}{}", URL_VIN, VIN_NOT_FOUND);
        try {
            restCaller.getByVin(VIN_NOT_FOUND, TOKEN);
        } catch (NotFoundException e) {
            assertNotNull(e);
        }
    }

}
