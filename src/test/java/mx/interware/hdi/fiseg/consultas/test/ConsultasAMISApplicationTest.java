package mx.interware.hdi.fiseg.consultas.test;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import mx.interware.hdi.fiseg.consultas.ConsultasAMISApplication;

//@ContextConfiguration(classes = { AppProperties.class })
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ExtendWith(MockitoExtension.class)
public class ConsultasAMISApplicationTest {
	private static Logger log = LogManager.getLogger(ConsultasAMISApplicationTest.class);

	//@Test
	public void contextLoads() {
		ApplicationContext ctx = SpringApplication.run(ConsultasAMISApplication.class);

		log.info("Testing beans ...");
		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			log.info("bean: " + beanName);
		}

	}

}
